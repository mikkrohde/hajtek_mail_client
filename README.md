# Manual for the Hajtek Mail Client
Welcome to the introduction guide for the Hajtek Mail Client. <br>
Here we will help you through setting up the Mail Client for first time users, as well as a reference point for any problems that might occur. <br>

The parts we will cover in this document will be:

1. Pre-requisites

2. Installation

3. Account Creation

4. FAQ and Support

## Pre-requisites
First there are some pre-requisites that are required to run the software, mainly the microsoft program .NET Framework version 4.7.2, which can be downloaded from [this link](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472). It should be noted that the required version will be the run-time version. Once this is downloaded you can move onto the next step.

## Installation
To install, first you have to download the zip-file and then unpack it. Afterwards there will be a shortcut that can be used to launch the application. You have now installed the program and are now ready to begin setting up an account.

## Account Creation
Once you have opened the application you will be presented with a page where you have to put in your information. When filling in the fields, you cannot leave an extra space behind the characters, since this will result in a non-functioning user. Fill in the required fields, and when filling in the SMTP- and IMAP-host fields, the email service will be given i.e. if you are using the AU post server, it will be post.au.dk and so on for other services. We currently only have support for AU post servers and Google's gmail servers. 

Once the information has been filled in properly, you will be asked to select an avatar for your user. Choose whichever you find most interesting. Once an avatar has been selected, the program will take a little time to download all the emails from the server of choice. While it is loading please do not click on the application as it will likely result in a crash. Once it has gotten all of the emails it will end the account creation process and prompt you to restart the application. Once the application is restarted you will be automatically logged in and can then use the service as it was intended. 

If you wish to have more than one profile, others can be created once you have logged in by going to the settings menu which can be accessed through the cogwheel at the top of the application.

## FAQ and Support
Changing profiles:
- If you click on your avatar you will have the opportunity to change between different profiles. These profiles can be created and deleted in the settings menu which can be accessed through the settings menu which is the small cogwheel icon at the top of the client.

Sending emails:
- Once an email has been sent, you will need to click on the inbox again to load it properly. This is unfortunately how it works and there has not yet been found a workaround for this, so we do apoligize for the inconvenience.

Marking emails:
- If you wish to mark an email as read or unread, you can right-click on the given email and then choose to mark it as the opposite of what it is currently. When right-clicking you can also choose to delete emails that will then get moved to the Trash folder.

Saving settings:
- If you change your settings in the client, to save these changes, you must close the application for that to take effect.

Extra information:
- If you open the task manager while the client is fetching the mails, the application will say that it is not responding. This is not something to raise concern over and should be ignored. 
- If your inbox is not showing properly, try clicking on the inbox logo on the left and then it will reload and show it properly.
- If you incorrectly setup your user, you can go into the Release folder and then delete the USER and PROFILE folders and then relaunch the program.
