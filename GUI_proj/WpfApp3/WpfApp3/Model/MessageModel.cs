﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp3.Model
{
    class MessageModel
    {
        public string FromUsername { get; set; }
        public string FromUsernameColor { get; set; }
        public string ImageSource { get; set; } 
        public string Message { get; set; }
        public DateTime Time { get; set; }
        public bool IsNativeOrigin { get; set; }
        public bool? FirstMessage { get; set; }

    }
}
