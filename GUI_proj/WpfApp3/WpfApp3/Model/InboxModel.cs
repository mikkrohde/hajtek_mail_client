﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;

namespace WpfApp3.Model
{
    class InboxModel
    {
        public string FromUsername { get; set; }
        public string ImageSource { get; set; }
        public string Subject { get; set; }
        public string Messages { get; set; }
        public DayOfWeek Time { get; set; }
        public string Date { get; set; }
        public int Mail_ID { get; set; }

    }
}
