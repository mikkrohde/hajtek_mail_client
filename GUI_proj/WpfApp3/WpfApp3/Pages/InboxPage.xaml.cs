﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;
using CefSharp;
using CefSharp.Internals;
using CefSharp.WinForms;
using WpfApp3.ViewModel;

namespace WpfApp3.Pages
{
    public partial class InboxPage : Page
    {
        public InboxPage()
        {
            InitializeComponent();
            OpenInbox();
        }



        private async void OpenInbox()
        {
            using (var client = new ImapClient())
            {
                client.Connect("post.au.dk", 993, true);

                client.Authenticate("AU634732", "Computeruni1818");

                //string messageid = ((EmailListData)(((ListBox)sender).SelectedItem)).Id;

                var inbox_folder = client.Inbox;

                await inbox_folder.OpenAsync(FolderAccess.ReadOnly);

                int mail_id_number = (inbox_folder.Count - 3);

                var message = inbox_folder.GetMessage(mail_id_number);

                message.

                mail_from.Content = message.From.OfType<MailboxAddress>().Single().Address;
                mail_to.Content = message.To.OfType<MailboxAddress>().Single().Address;

                cefBrowser.LoadHtml(message.HtmlBody, true);
               
                client.Disconnect(true);
            }
        }

        private void GetMail(int mail_id)
        {

        }


        public class EmailListData
        {

            public string Id { get; set; }
            public string Subject { get; set; }
        }

        private void ListBoxItem_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            MessageBox.Show("MouseDown event on " + Global.Mail_ID_List[0]);

        }



    }
}
