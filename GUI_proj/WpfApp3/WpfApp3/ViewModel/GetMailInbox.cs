﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp3.Model;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;

namespace WpfApp3.ViewModel
{
    class GetMailInbox
    {
        public ObservableCollection<MessageModel> Messages { get; set; }
        public ObservableCollection<InboxModel> Inboxes { get; set; }



        public GetMailInbox()
        {
            Messages = new ObservableCollection<MessageModel>();
            Inboxes = new ObservableCollection<InboxModel>();

            Messages.Add(new MessageModel
            {
                FromUsername = "Oliver Test",
                FromUsernameColor = "#14B0BF",
                ImageSource = "",
                Message = "123 Test",
                Time = DateTime.Now,
                IsNativeOrigin = false,
                FirstMessage = true
            });

            for (int i = 0; i < 3; i++)
            {
                Messages.Add(new MessageModel
                {
                    FromUsername = "Oliver Test",
                    FromUsernameColor = "#14B0BF",
                    ImageSource = "",
                    Message = "Last Test",
                    Time = DateTime.Now,
                    IsNativeOrigin = false
                });

            }

            //Mail in inbox cards

            for (int i = 0; i < 15; i++)
            {
                Inboxes.Add(new InboxModel
                {
                    FromUsername = $"Oliver Test {i}",
                    ImageSource = "",
                    Subject = "Subject",
                    //Messages = Messages,
                    Time = DateTime.Now.DayOfWeek
                });

            }
        }
}
}
