﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp3.Model;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;

namespace WpfApp3.ViewModel
{
    public class Global
    {
        public static List<int> Mail_ID_List { get; set; }
    }

    class MainViewModel
    {

        

        public ObservableCollection<InboxModel> Inboxes { get; set; }

        // public async void OpenInbox()

        public MainViewModel()
        {
            Inboxes = new ObservableCollection<InboxModel>();
            OpenInbox();

        }

        public async void OpenInbox()
        {
            Global.Mail_ID_List = new List<int>();

            using (var client = new ImapClient())
            {
                client.Connect("post.au.dk", 993, true);

                client.Authenticate("AU634732", "Computeruni1818");

                //string messageid = ((EmailListData)(((ListBox)sender).SelectedItem)).Id;

                var inbox_folder = client.Inbox;

                await inbox_folder.OpenAsync(FolderAccess.ReadOnly);

                //Mail in inbox cards

                for (int i = inbox_folder.Count-1; inbox_folder.Count - 25 < i; i--)
                {
                    Global.Mail_ID_List.Add(i);
                    var message = inbox_folder.GetMessage(i);
                    string from = message.From.OfType<MailboxAddress>().Single().Address.ToString();
                    from = from.Substring(0, 10);
                    string mail_subject = message.Subject;
                    //mail_subject = mail_subject.Substring(0, 5); //Begrænser længden på mail emne strengen. Virker ikke med et for højt tal :(

                    string date_time = message.Date.UtcDateTime.ToString();



                    Inboxes.Add(new InboxModel
                    {
                        FromUsername = from,
                        ImageSource = "",
                        Subject = mail_subject,
                        //Messages = "Messages",
                        Date = date_time,
                        Mail_ID = i

                    }); ; ; ;
                }




                    //mail_from.Content = message.From;
                    //mail_to.Content = message.To;

                    //textblock_body.Text = message.Subject;

                    client.Disconnect(true);
                }
        }


        /*
        public MainViewModel()
            {
                Inboxes = new ObservableCollection<InboxModel>();

                //Mail in inbox cards

                for (int i = 0; i < 15; i++)
                {
                    Inboxes.Add(new InboxModel
                    {
                        FromUsername = $"Oliver Test {i}",
                        ImageSource = "",
                        Subject = "Subject",
                        //Messages = "Messages",
                        Time = DateTime.Now.DayOfWeek
                    });

                }
        */


    }

    
}
