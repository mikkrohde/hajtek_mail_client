﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Hajtek_mail_client_dev.data_structures.email_data_structure
{
    public class UserData
    {
        public struct UserInfo
        {
            public string FirstName     { get; set; }           //0
            public string LastName      { get; set; }            //1
            public string FullName      { get; set; }            //2
            public string MailAddress   { get; set; }         //3
            public string AccountUsername { get; set; }     //4
            public string AccountPassword { get; set; }     //5
            public string Smtp_Host     { get; set; }            //6 (Sorry men det er nemmere når det hedder Host, for det er hvad det hedder i MailKit)
            public string Imap_Host     { get; set; }            //7 (Sorry men det er nemmere når det hedder Host, for det er hvad det hedder i MailKit)
            public string Pop3_Host     { get; set; }            //8 (Sorry men det er nemmere når det hedder Host, for det er hvad det hedder i MailKit)
            public int Smtp_PORT        { get; set; }              //9
            public int Imap_PORT        { get; set; }              //10
            public int Pop3_PORT        { get; set; }              //11
            public bool Use_ssl_IMAP    { get; set; }          //12
            public bool Use_ssl_SMTP    { get; set; }          //13
            public bool Use_german      { get; set; }
            public string AvatarName    { get; set; }         //14
            public int CurrMailCount    { get; set; }
            public int CurrMailUnReadCount { get; set; }
            public int CurrSentCount    { get; set; }
            public int CurrDraftCount   { get; set; }
            public int CurrSpamCount    { get; set; }
            public int CurrArchiveCount { get; set; }
            public int CurrTrashCount   { get; set; }
        }

        public class Data //Bruges til forskelligt data
        {
            public static List<string> Unread_Emails_Inbox = new List<string>();    //15    //Liste der gemmer antallet af ulæste emails (skal være en string!!)
            public static List<string> Unread_Emails_Spam = new List<string>();     //16    //Samme for spam
        
            public static List<int> Total_Emails_Inbox = new List<int>();           //17    //Liste der gemmer antallet af totale emails i inboxen (er int bare fordi det var nemmeste) (bruges ikke endnu)
            public static List<int> Total_Emails_Sent = new List<int>();            //18    //Samme for spam
            public static List<int> Total_Emails_Spam = new List<int>();            //19    //Samme for spam
            public static List<int> Total_Emails_Trash = new List<int>();           //20    //Samme for spam

            public static List<string> GetHTML_from_cef = new List<string>();       //21    //Reserveres til at kunne få HTML ud fra cefBrowseren (virker ikke endnu så bliver ikke brugt)

            public static ConcurrentBag<bool> Does_XML_exist = new ConcurrentBag<bool>();  //22  // ConcurrentBag, en slags liste, men er ikke index baseret. Minder om linked list/queue.
            //Bliver brugt til at gemme boolean værdien om XML filen findes, og gør det muligt at bruge blandt .cs filerne

            public static ConcurrentBag<string> Avatar_name = new ConcurrentBag<string>(); //23
        }

        public class Mailbox //Bruges til at gemme mail data
        {
            public List<UserInfo> Userdata;       //24
            public List<SendData> Senddata;       //25
            public List<InboxData> Inboxdata;     //26
            public List<SentData> Sentdata;       //27
            public List<SpamData> Spamdata;       //28
            public List<TrashData> Trashdata;    //29
        }

        public struct SendData //Struct til listerne overfor, og bruges til at gemme data til at sende mail
        {
            public string MailAddressOut { get; set; }        //30
            public string Subject { get; set; }     //31
            public string Body { get; set; }        //32
        }


        public struct InboxData //Struct til listerne overfor, og bruges til at gemme data om hver enkelt mail for inbox
        {
            public int Mail_ID { get; set; }            //33
            public string HTML_Body { get; set; }       //34
            public string Text_Body { get; set; }       //35
            public string Mail_From { get; set; }       //36
            public string Mail_From_EmailAddress { get; set; }       //36
            public string Mail_To { get; set; }         //37
            public string Mail_Subject { get; set; }    //38
            public string Mail_Message { get; set; }    //39
            public string Mail_Date_Time { get; set; }  //40
            public bool IsRead { get; set; }
        }

        public struct SentData //Struct til listerne overfor, og bruges til at gemme data om hver enkelt mail for sent
        {
            public int Mail_ID { get; set; }            //41
            public string HTML_Body { get; set; }       //42
            public string Text_Body { get; set; }       //43
            public string Mail_From { get; set; }       //44
            public string Mail_From_EmailAddress { get; set; }       //36
            public string Mail_To { get; set; }         //45
            public string Mail_Subject { get; set; }    //46
            public string Mail_Message { get; set; }    //47
            public string Mail_Date_Time { get; set; }  //48
            public bool IsRead { get; set; }          //49
        }
        

        public struct SpamData //Struct til listerne overfor, og bruges til at gemme data om hver enkelt mail for spam
        {
            public int Mail_ID { get; set; }            //50
            public string HTML_Body { get; set; }       //51
            public string Text_Body { get; set; }       //52
            public string Mail_From { get; set; }       //53
            public string Mail_From_EmailAddress { get; set; }
            public string Mail_To { get; set; }         //54
            public string Mail_Subject { get; set; }    //55
            public string Mail_Message { get; set; }    //56
            public string Mail_Date_Time { get; set; }  //57
            public bool IsRead { get; set; }          //58


        }

        public struct TrashData //Struct til listerne overfor, og bruges til at gemme data om hver enkelt mail for trash
        {
            public int Mail_ID { get; set; }            //59
            public string HTML_Body { get; set; }       //60
            public string Text_Body { get; set; }       //61
            public string Mail_From { get; set; }       //62
            public string Mail_From_EmailAddress { get; set; }
            public string Mail_To { get; set; }         //63
            public string Mail_Subject { get; set; }    //64
            public string Mail_Message { get; set; }    //65
            public string Mail_Date_Time { get; set; }  //66
            public bool IsRead { get; set; }          //67
        }

    }

}

