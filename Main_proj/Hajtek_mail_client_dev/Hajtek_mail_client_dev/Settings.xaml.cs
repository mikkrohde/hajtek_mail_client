﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Hajtek_mail_client_dev.libraries.setup_lib;

namespace Hajtek_mail_client_dev
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {

        public static string _avatar_picked;
        public string Avatar_picked
        {
            get { return _avatar_picked; }
            set { _avatar_picked = value; }
        }

        public static bool _useGerman;
        public bool UseGerman
        {
            get { return _useGerman; }
            set { _useGerman = value; }
        }

        public Settings()
        {
            InitializeComponent();

        }

        private void Account_password_textbox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Account_password_textbox_backtext.Visibility = Visibility.Collapsed;
        }
        

        private void Cancel_Delete_account_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Save_Settings_Click(object sender, RoutedEventArgs e)
        {
            

            string[] XML_elements = {First_name_textbox.Text,
                                    Last_name_textbox.Text,
                                    mail_address_textbox.Text,
                                    Account_name_textbox.Text,
                                    Account_password_textbox.Text,
                                    SMTP_host_textbox.Text,
                                    SMTP_port_textbox.Text,
                                    IMAP_host_textbox.Text,
                                    IMAP_port_textbox.Text,
                                    _avatar_picked,
                                    null};


            Core.SaveSettings(UseGerman, XML_elements, USE_SSL_SMTP_button.IsChecked, USE_SSL_IMAP_button.IsChecked);

            if (UseGerman == true)
            {
                MessageBox.Show("Ihre Änderungen ist speichert!");
            }
            else
            {
                MessageBox.Show("Your changes are saved!");
            }
        }

        private void Cancel_Save_Click(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Hidden;
        }

        private void Delete_all_accounts_Click(object sender, RoutedEventArgs e)
        {
            string users_path = Path.Combine(Core.CoreUserMBdata.LocalPaths.GetAppPath, "USERS");
            Directory.Delete(users_path, true);

            string profiles_path = Path.Combine(Core.CoreUserMBdata.LocalPaths.GetAppPath, "PROFILES");
            Directory.Delete(profiles_path, true);

            Properties.Settings.Default.Reset();
            Properties.Settings.Default.Save();

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                MessageBox.Show("Ihre User Informationen  ist erfolgreich gelöscht! Das Programm will  geschlossen.");
            }
            else
            {
                MessageBox.Show("Your user profiles are successfully deleted! The program will close.");
            }

            
            Application.Current.MainWindow.Close();
        }
        private void avatar1_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "1";
            avatar1.Background = Brushes.DarkGray;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar2_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "2";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.DarkGray;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar3_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "3";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.DarkGray;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar4_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "4";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.DarkGray;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar5_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "5";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.DarkGray;
        }

        private void german_selected_Click(object sender, RoutedEventArgs e)
        {
            UseGerman = true;
            german_selected.Background = Brushes.DarkGray;
            english_selected.Background = Brushes.Transparent;
        }

        private void english_selected_Click(object sender, RoutedEventArgs e)
        {
            UseGerman = false;
            german_selected.Background = Brushes.Transparent;
            english_selected.Background = Brushes.DarkGray;

        }
    }
}
