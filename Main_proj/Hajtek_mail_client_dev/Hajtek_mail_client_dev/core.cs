﻿using System;
using Hajtek_mail_client_dev.libraries.exceptions;
using Hajtek_mail_client_dev.libraries.setup_lib;
using Hajtek_mail_client_dev.libraries.email_transf_lib;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.dir_lib;
using System.Configuration;
using MimeKit.Encodings;
using MailKit;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using MimeKit;
using System.Threading.Tasks;
using System.Collections.Generic;
using MailKit.Net.Imap;
using System.Web;

namespace Hajtek_mail_client_dev
{

    /// <summary>
    /// Core functionality for the hajtek mail software
    /// </summary>
    /// <param name="none"></param>
    public class Core
    {
        /// <summary>
        /// Core Attributes - non accessable for user.
        /// </summary>
        private static UserData.UserInfo CoreActiveUser;
        public static UserData.Mailbox CoreMailbox = new UserData.Mailbox();
        private static int NewMailsReceived;

        public class CoreUserMBdata
        {
            public static string GetInboxCount          { get { return CoreActiveUser.CurrMailCount.ToString(); } }
            public static string GetInboxUnreadCount    { get { return CoreActiveUser.CurrMailUnReadCount.ToString(); } }
            public static string GetSentCount           { get { return CoreActiveUser.CurrSentCount.ToString(); } }
            public static string GetDraftCount          { get { return CoreActiveUser.CurrDraftCount.ToString(); } }
            public static string GetSpamCount           { get { return CoreActiveUser.CurrSpamCount.ToString(); } }
            public static string GetArchiveCount        { get { return CoreActiveUser.CurrArchiveCount.ToString(); } }
            public static string GetTrashCount          { get { return CoreActiveUser.CurrTrashCount.ToString(); } }
            public static string GetNewMails            { get { return NewMailsReceived.ToString(); } }
            public static string GetUserName            { get { return CoreActiveUser.AccountUsername; } }
            public static string GetUserPassword        { get { return CoreActiveUser.AccountPassword; } }
            public static string GetMailAddress         { get { return CoreActiveUser.MailAddress; } }
            public static string GetFullName            { get { return CoreActiveUser.FullName; } }
            public static string GetFirstName           { get { return CoreActiveUser.FirstName; } }
            public static string GetLastName            { get { return CoreActiveUser.LastName; } }
            public static bool GetUseGerman             { get { return CoreActiveUser.Use_german; } }
            public static bool GetUseSslSMTP            { get { return CoreActiveUser.Use_ssl_SMTP; } }
            public static bool GetUseSslIMAP            { get { return CoreActiveUser.Use_ssl_IMAP; } }
            public static string AvatarName             { get { return CoreActiveUser.AvatarName; } }
            public static string SMTPhost               { get { return CoreActiveUser.Smtp_Host; } }
            public static string IMAPhost               { get { return CoreActiveUser.Imap_Host; } }
            public static string SMTPport               { get { return CoreActiveUser.Smtp_PORT.ToString(); } }
            public static string IMAPport               { get { return CoreActiveUser.Imap_PORT.ToString(); } }


            public class LocalPaths
            {
                public static string GetInboxPath   { get { return Properties.Settings.Default.InboxPath; } }
                public static string GetSentPath    { get { return Properties.Settings.Default.SentPath; } }
                public static string GetDraftPath   { get { return Properties.Settings.Default.DraftPath; } }
                public static string GetSpamPath    { get { return Properties.Settings.Default.SpamPath; } }
                public static string GetTrashPath   { get { return Properties.Settings.Default.TrashPath; } }
                public static string GetAppPath   { get { return Application.StartupPath; } }
            }
        }
        

        /// <summary>
        /// Core Flags
        /// </summary>
        public class Flags
        {
            public static bool FinishedSTARTUP = false;
            public static bool Refresing = false;
            public static bool Sync = false;
        }

        /// <summary>
        /// This is the first method to run
        /// Performs:
        /// <list type="bullet">
        /// <item><param>Fetches last active user</param></item>
        /// <item><param>Loads the mails userdata.mailbox</param></item>
        /// <item><param></param></item>
        /// </list>
        /// </summary>
        public static void STARTUP()
        {

            //Fetch Previous active user in the hajtek mailclient
            using (Imap_setup inits = new Imap_setup()){
                CoreActiveUser = inits.Init();
            }

            //Fetches new mails from IMAP server
            using(IRecieveMail FetchNewMail = new DefImapRecieve(CoreActiveUser))
            {
                try
                {
                    NewMailsReceived = FetchNewMail.Receive_mail(ref CoreActiveUser);

                    CoreActiveUser.CurrMailCount += NewMailsReceived;

                    FetchNewMail.Receive_mail(ref CoreActiveUser, true, "Sent", "Trash", "Junk");
                    
                }
                catch(Exception ex)
                {
                    //dont care.
                }

                SAVE();
            }

            //Pulls all mails into the lists in UserData.Mailbox
            using(IMailOP LoadMailList = new ImapMailOP(CoreActiveUser))
            {
                LoadMailList.LoadLocal_Inbox(ref CoreMailbox);
                LoadMailList.LoadLocal_Sent(ref CoreMailbox);
                LoadMailList.LoadLocal_Spam(ref CoreMailbox);
                LoadMailList.LoadLocal_Trash(ref CoreMailbox);
            }

            Flags.FinishedSTARTUP = true;
        }

        /// <summary>
        /// This method is the last to run when exiting the program properly
        /// </summary>
        public static void SAVE()
        {
            //Runs when clossing the
            using(Imap_setup StoreNewXmlData = new Imap_setup())
            {
                string StringMailCount = CoreActiveUser.CurrMailCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrMailCount", StringMailCount);

                string StringMailUnReadCount = CoreActiveUser.CurrMailUnReadCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrMailUnReadCount", StringMailUnReadCount);

                StringMailCount = CoreActiveUser.CurrSentCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrSentCount", StringMailCount);
                
                StringMailCount = CoreActiveUser.CurrDraftCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrDraftCount", StringMailCount);
                
                StringMailCount = CoreActiveUser.CurrArchiveCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrArchiveCount", StringMailCount);
                
                StringMailCount = CoreActiveUser.CurrTrashCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrTrashCount", StringMailCount);

                StringMailCount = CoreActiveUser.CurrSpamCount.ToString();
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "CurrSpamCount", StringMailCount);
            }
        }


        public static void AddAvatarNameToXML(string AvatarName)
        {
            using (Imap_setup StoreNewXmlData = new Imap_setup())
            {
                StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "AvatarName", AvatarName);
            }
        }

        public static void SaveSettings(bool UseGerman, string[] XML_element, bool? SSL_SMTP, bool? SSL_IMAP)
        {
            using (Imap_setup StoreNewXmlData = new Imap_setup())
            {
               
                    if (UseGerman == true)
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_german", "true");
                    }
                    else
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_german", "false");
                    }
                    
                

                if (XML_element[0] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "FirstName", XML_element[0]);
                }

                if (XML_element[1] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "LastName", XML_element[1]);
                }

                string Fullname = XML_element[0] + " " + XML_element[1];
                if (Fullname != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Fullname", Fullname);
                }

                if (XML_element[2] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "MailAddress", XML_element[2]);
                }

                if (XML_element[3] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "AccountUsername", XML_element[3]);
                }

                if (XML_element[4] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "AccountPassword", XML_element[4]);
                }

                if (XML_element[5] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Smtp_Host", XML_element[5]);
                }

                if (XML_element[6] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Smtp_PORT", XML_element[6]);
                }

                if (XML_element[7] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Imap_Host", XML_element[7]);
                }

                if (XML_element[8] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Imap_PORT", XML_element[8]);
                }

                if (XML_element[9] != "")
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "AvatarName", XML_element[9]);
                }
                else
                {
                    StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "AvatarName", CoreActiveUser.AvatarName);
                }

               
                    if (SSL_SMTP == true)
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_ssl_SMTP", "true");
                    }
                    else
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_ssl_SMTP", "false");
                    }
                    
                

                
                    if (SSL_IMAP == true)
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_ssl_IMAP", "true");
                    }
                    else
                    {
                        StoreNewXmlData.UpdateXMLInnerText(CoreActiveUser, "Use_ssl_IMAP", "false");
                    }
                    
                



                
                


            }
        }

        /// <summary>
        /// Refreshes the loaded amount of mails.
        /// </summary>
        public static void REFRESH()
        {
            Flags.Refresing = true;

            try
            {
                //Pulls all mails into the lists in UserData.Mailbox
                using (IMailOP LoadMailList = new ImapMailOP(CoreActiveUser))
                {
                    LoadMailList.LoadLocal_Inbox(ref CoreMailbox);
                    LoadMailList.LoadLocal_Sent(ref CoreMailbox);
                    LoadMailList.LoadLocal_Spam(ref CoreMailbox);
                    LoadMailList.LoadLocal_Trash(ref CoreMailbox);
                }
            }
            catch(IndexOutOfRangeException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            catch(System.Xaml.XamlParseException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            catch (System.Xaml.XamlException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            finally
            {
                Flags.Refresing = false;
            }
        }


        public static void SYNCRONIZE()
        {
            Flags.Sync = true;

            try
            {
                //Fetches new mails from IMAP server
                using (IRecieveMail FetchNewMail = new DefImapRecieve(CoreActiveUser))
                {
                    NewMailsReceived = FetchNewMail.Receive_mail(ref CoreActiveUser);
                    CoreActiveUser.CurrMailCount += NewMailsReceived;

                    FetchNewMail.Receive_mail(ref CoreActiveUser, true, "Sent", "Trash", "Junk");

                    SAVE();
                }

                //Pulls all mails into the lists in UserData.Mailbox
                using (IMailOP LoadMailList = new ImapMailOP(CoreActiveUser))
                {
                    LoadMailList.LoadLocal_Inbox(ref CoreMailbox);
                    LoadMailList.LoadLocal_Sent(ref CoreMailbox);
                    LoadMailList.LoadLocal_Spam(ref CoreMailbox);
                    LoadMailList.LoadLocal_Trash(ref CoreMailbox);
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            catch (System.Xaml.XamlParseException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            catch (System.Xaml.XamlException ex)
            {
                throw new CoreExceptions.RefreshIndexexception(ex.HelpLink);
            }
            finally
            {
                Flags.Sync = false;
            }
        }


        #region Setup & User Operations

        /// <summary>
        /// First time setup
        /// Is a method called at first time setup, to setup settings.
        /// </summary>
        /// <returns>Mailbox_data</returns>
        public static void FirstTimeSetup()
        {
            //Remember to create this folder somehow
            string newuserspath = Path.Combine(Application.StartupPath, "USERS");
            string newprofilepath = Path.Combine(Application.StartupPath, "PROFILES");
            Directory.CreateDirectory(newuserspath);
            Directory.CreateDirectory(newprofilepath);
        }

        /// <summary>
        /// Creates a new user
        /// - arguments are ui elements with values
        /// <list type="number">
        /// <item><param name="T">IMAP SSL USE</param></item>
        /// <item><param name="T">SMTP SSL USE</param></item>
        /// <item><param name="T">First name</param></item>
        /// <item><param name="T">Last name</param></item>
        /// <item><param name="T">Mail Address</param></item>
        /// <item><param name="T">Account username</param></item>
        /// <item><param name="T">Account password</param></item>
        /// <item><param name="T">SMTP address</param></item>
        /// <item><param name="T">IMAP address</param></item>
        /// <item><param name="T">SMTP port</param></item>
        /// <item><param name="T">IMAP port</param></item>
        /// </list>
        /// </summary>
        /// <param name="_UIelements"></param>
        public static void CreateNewUser(bool imapssl, bool smtpssl, bool useGerman, params string[] _UIelements)
        {
            DirMgmtLocal dir = new DirMgmtLocal();
            UserData.UserInfo NewUser = new UserData.UserInfo();

            using (Imap_setup setup = new Imap_setup())
            {
                UserData.UserInfo tmptest = new UserData.UserInfo();
                tmptest.AccountUsername = _UIelements[3];
                tmptest.AccountPassword = _UIelements[4];
                tmptest.Imap_Host = _UIelements[6];

                if (int.TryParse(_UIelements[8], out int res0))
                {
                    tmptest.Imap_PORT = res0;
                }

                tmptest.Smtp_Host = _UIelements[5];

                if (int.TryParse(_UIelements[7], out int res1))
                {
                    tmptest.Smtp_PORT = res1;
                }

                try {
                    setup.TestImapConnection(tmptest);
                    setup.TestSMTPConnection(tmptest);
                }
                catch (Exception ex) when (ex is MailKit.Security.AuthenticationException||
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentException)
                {
                    throw;
                }

                setup.NewUser(NewUser, imapssl, smtpssl, useGerman, _UIelements);
            }

            //Switches to the newly created account
            //Fetches the newly created users AccountFullname directly from the gui elements.
            var tmpNewUser = _UIelements[0] + _UIelements[1];

            //pulls username from gui
            var tmpNewAccUsername = _UIelements[3];

            SwitchUser(tmpNewUser, tmpNewAccUsername);

            return;
        }


        /// <summary>
        /// Switches the previous active user with a new active user.
        /// <list type="bullet">
        /// <item>Perfoms a switch of the CoreActiveUser in runtime.</item>
        /// <item>Saves the CoreActiveUser's new data in the matching XML profile</item>
        /// </list>
        /// </summary>
        /// <param name="NewConnUser"></param>
        /// <param name="AccUsername"></param>
        public static void SwitchUser(string NewConnUser, string AccUsername)
        {

            Properties.Settings DynSettings = Properties.Settings.Default;
            DirMgmtLocal dir = new DirMgmtLocal();

            var ActiveUsersName = CoreActiveUser.FirstName + CoreActiveUser.LastName;

            if(ActiveUsersName == NewConnUser && CoreActiveUser.AccountUsername == AccUsername)
            {
                return;
            }

            SAVE();

            //Gets the paths for the folders in the users mailbox 
            string partialpath = Path.Combine("USERS", AccUsername);
            string[] UserMailboxfolders = dir.GetSubFolders(partialpath);

            //stores current active user as a tmp variable
            //UserData.UserInfo StoreOldUser = CoreActiveUser;

            //creates temporary for new active user
            UserData.UserInfo NewActiveUser;

            using (var FetchSwitchUser = new Imap_setup())
            {
                //FetchSwitchUser.UpdateUserXML();
                NewActiveUser = FetchSwitchUser.LoadNewActiveUser(NewConnUser);
            }
            DynSettings.ActiveUser = NewActiveUser.FirstName + NewActiveUser.LastName;
            CoreActiveUser = NewActiveUser;

            foreach (string Mailboxfolder in UserMailboxfolders)
            {
                if (Path.GetFileName(Mailboxfolder) == "Inbox")
                {
                    DynSettings.InboxPath = Mailboxfolder;
                }
                if (Path.GetFileName(Mailboxfolder) == "Sent")
                {
                    DynSettings.SentPath = Mailboxfolder;
                }
                if (Path.GetFileName(Mailboxfolder) == "Draft")
                {
                    DynSettings.DraftPath = Mailboxfolder;
                }
                if (Path.GetFileName(Mailboxfolder) == "Spam")
                {
                    DynSettings.SpamPath = Mailboxfolder;
                }
                if (Path.GetFileName(Mailboxfolder) == "Trash")
                {
                    DynSettings.TrashPath = Mailboxfolder;
                }
            }

            DynSettings.Save();

            SAVE();
        }

        public static void RESTARTAPP()
        {
            Application.Restart();
            Environment.Exit(0);
        }

        #endregion


        #region Mail Fetch & Send Operations

        public static void SendMail<T>(string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI) 
        { 
            using (ISendMail<T> sendmail = new SmtpSend<T>(CoreActiveUser, ref CoreMailbox))
            {
                try {
                    sendmail.Send_mail(mailheaderUI, subjectheaderUI, mailtextbodyUI);
                }
                catch(CoreExceptions)
                {
                    throw;
                }
            }
        }






        public static void ReplyToMail<T>(List<T> mailboxlist, int mailID, string subjectheaderUI, string mailtextbodyUI)
        {
            using (ISendMail<T> sendmail = new SmtpSend<T>(CoreActiveUser, ref CoreMailbox))
            {
                try {
                sendmail.Reply_To_mail(mailboxlist, mailID, subjectheaderUI, mailtextbodyUI);
                }
                catch (CoreExceptions)
                {
                throw;
                }
            }
        }

        public static void ForwardMail<T>(List<T> mailboxlist, int mailID, string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI)
        {
            using (ISendMail<T> sendmail = new SmtpSend<T>(CoreActiveUser, ref CoreMailbox))
            {
                try
                {
                    sendmail.Forward_mail(mailboxlist, mailID, mailheaderUI, subjectheaderUI, mailtextbodyUI);
                }
                catch (CoreExceptions)
                {
                    throw;
                }
            }
        }

        public static int GetNewMail()
        {
            int mails = 0;
            using (IRecieveMail client = new DefImapRecieve(CoreActiveUser))
            {
                mails = client.Receive_mail(ref CoreActiveUser); //Virker ikke fordi der står uint og Recieve_mail bruger int
            }

            return mails;
        }

        #endregion


        public static MimeMessage FullMailLoad(string path, string mail_id)
        {
            MimeMessage NewlyLoadedMessage;

            using (IMailOP LoadFullMail = new ImapMailOP(CoreActiveUser))
            {
                var FetchSpecMail = LoadFullMail.Load_Mail(path, mail_id);
                NewlyLoadedMessage = FetchSpecMail;
            }

            return NewlyLoadedMessage;
        }

        public static bool MoveToTrash(string oldmailpath, string UniqueMailID)
        {
            using(IMailOP delete = new ImapMailOP(CoreActiveUser))
            {
                if(delete.MoveToTrashFolder(oldmailpath, UniqueMailID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void CompletelyDeleteMail(int selectedfolder, int mailid)
        {
            using(ImapClient CDmail = new ImapClient())
            {
                try
                {
                    CDmail.Connect(CoreActiveUser.Imap_Host, CoreActiveUser.Imap_PORT, MailKit.Security.SecureSocketOptions.SslOnConnect);
                    CDmail.Authenticate(CoreActiveUser.AccountUsername, CoreActiveUser.AccountPassword);
                }catch(Exception ex) when (ex is System.Net.Sockets.SocketException ||
                ex is ArgumentNullException|| 
                ex is IOException ||
                ex is ArgumentException ||
                ex is ImapProtocolException ||
                ex is MailKit.Security.AuthenticationException ||
                ex is OperationCanceledException
                )
                {
                    throw new CoreExceptions.ConnectionException();
                }

                IMailFolder activeimapfolder;
                Properties.Settings dynpaths = Properties.Settings.Default;

                if (CDmail.Capabilities.HasFlag(ImapCapabilities.UidPlus))
                {
                    switch (selectedfolder)
                    {
                    case 1:
                            activeimapfolder = CDmail.Inbox;

                            UniqueId uid1 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid1, MessageFlags.Deleted, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid1 });

                            break;
                    case 2:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Sent);

                            UniqueId uid2 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid2, MessageFlags.Deleted, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid2 });

                            break;
                    case 3:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Junk);

                            UniqueId uid3 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid3, MessageFlags.Deleted, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid3 });
                            break;
                    case 4:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Trash);

                            UniqueId uid4 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid4, MessageFlags.Deleted, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid4 });
                            break;
                    }
                }

                if (selectedfolder == 1)
                {
                    var filepath = dynpaths.InboxPath;
                    DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                    FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(mailid.ToString() + "*");

                    var t = filesInDir;

                    File.Delete(filesInDir[0].FullName);

                }
                else if(selectedfolder == 2)
                {
                    var filepath = dynpaths.SentPath;
                    DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                    FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(mailid.ToString() + "*");

                    var t = filesInDir;

                    File.Delete(filesInDir[0].FullName);

                }
                else if(selectedfolder == 3)
                {
                    var filepath = dynpaths.SpamPath;
                    DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                    FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(mailid.ToString() + "*");

                    var t = filesInDir;

                    File.Delete(filesInDir[0].FullName);


                }
                else if(selectedfolder == 4)
                {
                    var filepath = dynpaths.TrashPath;
                    DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                    FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(mailid.ToString() + "*");

                    var t = filesInDir;

                    File.Delete(filesInDir[0].FullName);
                }

                REFRESH();

            }
        }

        public static void SetSeenFlagOnMailServer(int selectedfolder, int mailid)
        {
            using (ImapClient CDmail = new ImapClient())
            {
                try
                {
                    CDmail.Connect(CoreActiveUser.Imap_Host, CoreActiveUser.Imap_PORT, MailKit.Security.SecureSocketOptions.SslOnConnect);
                    CDmail.Authenticate(CoreActiveUser.AccountUsername, CoreActiveUser.AccountPassword);
                }
                catch (Exception ex) 
                when (
                    ex is System.Net.Sockets.SocketException ||
                    ex is ArgumentNullException ||
                    ex is IOException ||
                    ex is ArgumentException ||
                    ex is ImapProtocolException ||
                    ex is MailKit.Security.AuthenticationException ||
                    ex is OperationCanceledException
                )
                {
                    throw new CoreExceptions.ConnectionException();
                }

                IMailFolder activeimapfolder;
                Properties.Settings dynpaths = Properties.Settings.Default;

                if (CDmail.Capabilities.HasFlag(ImapCapabilities.UidPlus))
                {
                    switch (selectedfolder)
                    {
                        case 1:
                            activeimapfolder = CDmail.Inbox;

                            UniqueId uid1 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid1, MessageFlags.Seen, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid1 });

                            break;
                        case 2:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Sent);

                            UniqueId uid2 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid2, MessageFlags.Seen, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid2 });

                            break;
                        case 3:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Junk);

                            UniqueId uid3 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid3, MessageFlags.Seen, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid3 });
                            break;
                        case 4:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Trash);

                            UniqueId uid4 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid4, MessageFlags.Seen, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid4 });
                            break;
                    }
                }

                REFRESH();

            }
        }

        public static void SetUnSeenFlagOnMailServer(int selectedfolder, int mailid)
        {
            using (ImapClient CDmail = new ImapClient())
            {
                try
                {
                    CDmail.Connect(CoreActiveUser.Imap_Host, CoreActiveUser.Imap_PORT, MailKit.Security.SecureSocketOptions.SslOnConnect);
                    CDmail.Authenticate(CoreActiveUser.AccountUsername, CoreActiveUser.AccountPassword);
                }
                catch (Exception ex) when (ex is System.Net.Sockets.SocketException ||
               ex is ArgumentNullException ||
               ex is IOException ||
               ex is ArgumentException ||
               ex is ImapProtocolException ||
               ex is MailKit.Security.AuthenticationException ||
               ex is OperationCanceledException
               )
                {
                    throw new CoreExceptions.ConnectionException();
                }

                IMailFolder activeimapfolder;
                Properties.Settings dynpaths = Properties.Settings.Default;

                if (CDmail.Capabilities.HasFlag(ImapCapabilities.UidPlus))
                {
                    switch (selectedfolder)
                    {
                        case 1:
                            activeimapfolder = CDmail.Inbox;

                            UniqueId uid1 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid1, MessageFlags.None, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid1 });

                            break;
                        case 2:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Sent);

                            UniqueId uid2 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid2, MessageFlags.None, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid2 });

                            break;
                        case 3:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Junk);

                            UniqueId uid3 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid3, MessageFlags.None, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid3 });
                            break;
                        case 4:
                            activeimapfolder = CDmail.GetFolder(SpecialFolder.Trash);

                            UniqueId uid4 = new UniqueId((uint)mailid);

                            activeimapfolder.Open(FolderAccess.ReadWrite);
                            activeimapfolder.SetFlags(uid4, MessageFlags.None, true);
                            activeimapfolder.Expunge(new UniqueId[] { uid4 });
                            break;
                    }
                }

                REFRESH();

            }
        }



        public static void ChangeFilenameFlagToIsRead(string folderpath, string filename)
        {

            FileInfo[] FilesInDir;
            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(folderpath);

            try
            {
                FilesInDir = DirectoryInWhichToSearch.GetFiles(filename + "_*");
                string getfullname = FilesInDir[0].FullName;
            }
            catch (Exception ex)
            when (
            ex is System.Xaml.XamlParseException ||
            ex is IOException || ex is ArgumentNullException)
            {
                //rethrows for exception handling in Core
                throw;
            }

            string full_path_name = FilesInDir[0].FullName;
            string full_filename = FilesInDir[0].Name;

            string[] filename_split = full_filename.Split('_', '.');

            int new_flag_val = Int32.Parse(filename_split[1]) + 1;

            filename_split[1] = new_flag_val.ToString();

            string new_filename = filename_split[0] + "_" + filename_split[1] + "." + filename_split[2];

            
            string src = full_path_name;
            string dest = Path.Combine(folderpath, new_filename);

            try
            {
                File.Move(src, dest);

                if (!File.Exists(src))
                {
                }
            }
            catch (IOException e)
            {
            }
        }

        public static string TextToHtml(string text)       //Get used when mail is in plain text and not HTML.
        {                                            //The cefbrowser can only show HTML, therefore a conversion from text to HTML is needed.
            text = HttpUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            return text;
        }

        public static void ChangeFilenameFlagToUnRead(string folderpath, string filename)
        {

            FileInfo[] FilesInDir;
            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(folderpath);

            try
            {
                FilesInDir = DirectoryInWhichToSearch.GetFiles(filename + "_*");
                string getfullname = FilesInDir[0].FullName;
            }
            catch (Exception ex)
            when (
            ex is System.Xaml.XamlParseException ||
            ex is IOException || ex is ArgumentNullException)
            {
                //rethrows for exception handling in Core
                throw;
            }

            string full_path_name = FilesInDir[0].FullName;
            string full_filename = FilesInDir[0].Name;

            string[] filename_split = full_filename.Split('_', '.');

            int new_flag_val = Int32.Parse(filename_split[1]) - 1;

            filename_split[1] = new_flag_val.ToString();

            string new_filename = filename_split[0] + "_" + filename_split[1] + "." + filename_split[2];


            string src = full_path_name;
            string dest = Path.Combine(folderpath, new_filename);

            try
            {
                File.Move(src, dest);

                if (!File.Exists(src))
                {
                }
            }
            catch (IOException e)
            {
            }

        }



    

    }
}