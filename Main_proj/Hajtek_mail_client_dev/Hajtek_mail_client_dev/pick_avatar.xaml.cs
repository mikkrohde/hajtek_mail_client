﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hajtek_mail_client_dev;
using Hajtek_mail_client_dev.libraries.setup_lib;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using System.ComponentModel;

namespace Hajtek_mail_client_dev
{
    /// <summary>
    /// Interaction logic for pick_language.xaml
    /// </summary>
    public partial class pick_avatar : UserControl
    {
        public pick_avatar()
        {
            InitializeComponent();
        }

        public void Show_avatar_self()
        {
            Visibility = Visibility.Visible;
        }
        
        private void avatar1_Click(object sender, RoutedEventArgs e)
        {
            Core.STARTUP();
            Core.AddAvatarNameToXML("1");
            Visibility = Visibility.Hidden;
        }

        private void avatar2_Click(object sender, RoutedEventArgs e)
        {
            Core.STARTUP();
            Core.AddAvatarNameToXML("2");
            Visibility = Visibility.Hidden;
        }

        private void avatar3_Click(object sender, RoutedEventArgs e)
        {
            Core.STARTUP();
            Core.AddAvatarNameToXML("3");
            Visibility = Visibility.Hidden;
        }

        private void avatar4_Click(object sender, RoutedEventArgs e)
        {
            Core.STARTUP();
            Core.AddAvatarNameToXML("4");
            Visibility = Visibility.Hidden;
        }

        private void avatar5_Click(object sender, RoutedEventArgs e)
        {
            Core.STARTUP();
            Core.AddAvatarNameToXML("5");
            Visibility = Visibility.Hidden;
        }
    }
}
