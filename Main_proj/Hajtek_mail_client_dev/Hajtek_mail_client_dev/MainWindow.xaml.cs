﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Hajtek_mail_client_dev.libraries.setup_lib;
using Hajtek_mail_client_dev.Model;
using CefSharp;
using CefSharp.Web;
using CefSharp.Internals;
using CefSharp.WinForms;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;
using Hajtek_mail_client_dev.libraries.ScrollViewer;
using System.Collections.ObjectModel;
using Hajtek_mail_client_dev.libraries.email_transf_lib;
using Hajtek_mail_client_dev.libraries.dir_lib;
using System.IO;
using System.ComponentModel;
using System.Xml.Serialization;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.truncate;
using Hajtek_mail_client_dev;
using Hajtek_mail_client_dev.libraries.exceptions;
using MailKit.Security;

namespace Hajtek_mail_client_dev
{
    public partial class MainWindow : UserControl
    {
        public ObservableCollection<InboxModel> _collection = new ObservableCollection<InboxModel>(); //Bruges til indbox cards i listen med mails
        public ObservableCollection<InboxModel> Collection //Bruges til indbox cards i listen med mails, men gør det muligt at vælge mellem at vise mails for inbox, sent, spam og trash
        {
            get { return _collection; }
            set {
                _collection = value;               
            }
        }

        public static int Mail_ID;

        public static int _selected_folder;

        public int Selected_folder
        {
            get { return _selected_folder; }
        }

        public bool Show_cefBrowser
        {
            get { return Hajtek_mail_client_dev.NewPopup._show_cefBrowser; }
        }

        public static int new_reply_forward;



        /* Stuff to fix.
         *  2. Left click in switch user gui element causes switch user method call. even without being left clicking on an user.
         *  3. trash needs to get removed the isread flags
         *  3. move to trash and then delete.
         *  4. check offline capabilities
         *  5. Reply contains mail to inputfield
         *  6. Make Language localization
         *  7. add expunge to clean deleted mails from both server and local storage
         */

        public MainWindow()
        {
            //Currently used for reseting Default inside app.config file.
            //Properties.Settings.Default.Reset();
            //Properties.Settings.Default.Save();

            InitializeComponent(); //Kører xaml

            
            if (Properties.Settings.Default.FirstLaunch == false)
            {
                Core.STARTUP();

                if (Core.CoreUserMBdata.GetUseGerman == true)
                {
                    IfGerman();
                }
                
                RunDefault();
                FillSettingsInfo();

            }
        }

     

        public void IfGerman()
        {
            rdInbox.Content = "Inbox";
            rdSent.Content = "Gesendet";
            rdSpam.Content = "Spam";
            rdTrash.Content = "Mülleimer";
            ListView_Name.Content = "Inbox";
            New.Content = "Neue Mail";
            From_info.Content = "Absender:";
            To_info.Content = "Empfänger:";
            Reply_button.Content = "Beantworten";
            Forward_button.Content = "Weiter senden";

            SwitchUser_popup.SwitchUser_title.Content = "Tauscht User";
            SwitchUser_popup.Close_NewPopup.Content = "Schloss";
            SwitchUser_popup.NewUser_button.Content = "Neue User";

            NewPopup.Close_NewPopup.Content = "Schloss";
            NewPopup.Send.Content = "Senden";

            Settings_popup.Done_text.Text = "Einstellungen";
            Settings_popup.Done_text_medium.Text = "Du kannst deine User Informationen hier verändert";
            Settings_popup.Done_text_small_first.Text = "Bei drucken 'Speichern', du überschreibt alle Informationen früher in den Feld.";
            Settings_popup.Done_text_small_second.Text = "Bei die 'löschen alle User' drücken, du löscht alle User und Mails du örtlich an dem Computer speichern hat. Dies hat keinen Einfluss auf ihr Mail Konto.";
            Settings_popup.Save_Settings.Content = "Speichern";
            Settings_popup.Cancel_Save.Content = "Abbrechen";
            Settings_popup.Delete_all_accounts.Content = "Löschen alle User";

            Settings_popup.First_name_textbox_backtext.Text = "Vorname";
            Settings_popup.Last_name_textbox_backtext.Text = "Nachname";
            Settings_popup.mail_address_textbox_backtext.Text = "E-Mail-Addresse";
            Settings_popup.Account_name_textbox_backtext.Text = "Benutzername";
            Settings_popup.Account_password_textbox_backtext.Text = "Passwort";
            Settings_popup.SMTP_host_textbox_backtext.Text = "SMTP Gastgeber";
            Settings_popup.SMTP_port_textbox_backtext.Text = "SMTP Anschlussstelle";
            Settings_popup.USE_SSL_SMTP_button.Content = "SSL für SMTP verwenden?";
            Settings_popup.IMAP_host_textbox_backtext.Text = "IMAP Gastgeber";
            Settings_popup.IMAP_port_textbox_backtext.Text = "IMAP Anschlussstelle";
            Settings_popup.USE_SSL_IMAP_button.Content = "SSL für IMAP verwenden?";

            


        }

        private void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                MessageBox.Show("Ändert!");
            }
            else
            {
                MessageBox.Show("Changed!");
            }

            
        }

        public void FillSettingsInfo()
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                Settings_popup.german_selected.Background = Brushes.DarkGray;
            }
            else
            {
                Settings_popup.english_selected.Background = Brushes.DarkGray;
            }

            Settings_popup.First_name_textbox_backtext.Text = Core.CoreUserMBdata.GetFirstName;
            Settings_popup.Last_name_textbox_backtext.Text = Core.CoreUserMBdata.GetLastName;
            Settings_popup.mail_address_textbox_backtext.Text = Core.CoreUserMBdata.GetMailAddress;
            Settings_popup.Account_name_textbox_backtext.Text = Core.CoreUserMBdata.GetUserName;
            Settings_popup.Account_password_textbox_backtext.Text = Core.CoreUserMBdata.GetUserPassword;
            Settings_popup.SMTP_host_textbox_backtext.Text = Core.CoreUserMBdata.SMTPhost;
            Settings_popup.SMTP_port_textbox_backtext.Text = Core.CoreUserMBdata.SMTPport;
            Settings_popup.USE_SSL_SMTP_button.IsChecked = Core.CoreUserMBdata.GetUseSslSMTP;
            Settings_popup.IMAP_host_textbox_backtext.Text = Core.CoreUserMBdata.IMAPhost;
            Settings_popup.IMAP_port_textbox_backtext.Text = Core.CoreUserMBdata.IMAPport;
            Settings_popup.USE_SSL_IMAP_button.IsChecked = Core.CoreUserMBdata.GetUseSslIMAP;
            
            if (Core.CoreUserMBdata.AvatarName == "1")
            {
                Settings_popup.avatar1.Background = Brushes.DarkGray;
            }
            if (Core.CoreUserMBdata.AvatarName == "2")
            {
                Settings_popup.avatar2.Background = Brushes.DarkGray;
            }
            if (Core.CoreUserMBdata.AvatarName == "3")
            {
                Settings_popup.avatar3.Background = Brushes.DarkGray;
            }
            if (Core.CoreUserMBdata.AvatarName == "4")
            {
                Settings_popup.avatar4.Background = Brushes.DarkGray;
            }
            if (Core.CoreUserMBdata.AvatarName == "5")
            {
                Settings_popup.avatar5.Background = Brushes.DarkGray;
            }
        }

        public void RunDefault()
        {
            cefBrowser.BrowserSettings = new BrowserSettings //Sætter utf-8 support til i cefBrowseren
            {
                DefaultEncoding = "iso-8859-1"
            };

            //MessageBox.Show(cefBrowser.BrowserSettings.DefaultEncoding);

            _selected_folder = 1; //1 = inbox
            _collection.Clear(); //Funktion der fjerner de ting der var i inbox card så det nedenfor kan indsættes i dem og vises

            try
            {
                if (Core.CoreMailbox.Inboxdata != null && Core.CoreMailbox.Inboxdata.Count > 0)
                {
                    cefBrowser_Winforms.Visibility = Visibility.Visible;
                    int index = Core.CoreMailbox.Inboxdata.Count() - 1;
                    string mailid = Core.CoreMailbox.Inboxdata[index].Mail_ID.ToString();
                    Mail_ID = int.Parse(mailid);
                    var fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetInboxPath, mailid);

                    NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                    mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                    mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen

                    Subject_mail.Content = fetchmessage.Subject;
                    DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    

                    if (fetchmessage.HtmlBody == null)
                    {
                        cefBrowser.LoadHtml(fetchmessage.TextBody);
                    }
                    else
                    {
                        cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                    }

                    _collection.Clear();
                    for (int i = Core.CoreMailbox.Inboxdata.Count - 1; 0 < i; --i)
                    {
                        if (Core.CoreMailbox.Inboxdata[i].Mail_Date_Time != null)
                        {
                            _collection.Add(new InboxModel
                            {
                                FromUsername = Core.CoreMailbox.Inboxdata[i].Mail_From.Truncate_func(35),
                                Subject = Core.CoreMailbox.Inboxdata[i].Mail_Subject,
                                Date_Time = Core.CoreMailbox.Inboxdata[i].Mail_Date_Time,
                                Mail_ID = Core.CoreMailbox.Inboxdata[i].Mail_ID,
                                IsRead = Core.CoreMailbox.Inboxdata[i].IsRead
                            });
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("No messages could be loaded: Trying again");
                Core.STARTUP();
            }

            rdInbox.DataContext = Core.CoreUserMBdata.GetInboxUnreadCount.ToString(); //Number of unread eamil displayed in UI
            //rdSpam.DataContext = UserData.Data.Unread_Emails_Spam[0]; //Number of unread eamil displayed in UI

            //int index1 = Core.CoreMailbox.Inboxdata.Count() - 1;
            //string checkinboxCount = Core.CoreMailbox.Inboxdata[index1].Mail_ID.ToString();

            //var FirstMail = Core.FullMailLoad(Properties.Settings.Default.InboxPath, checkinboxCount);

            //mail_from.Content = FirstMail.From[0].Name + "<" + FirstMail.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
            //mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen


            //cefBrowser.LoadHtml(FirstMail.HtmlBody, true); //Viser den pågældende email (i HTML format) i cefBrowseren

            UserAccount_Name.Content = Core.CoreUserMBdata.GetFullName; //Viser navnet på brugeren der er logget ind (nederste vestre hjørne)
            UserAccount_MailAddress.Content = Core.CoreUserMBdata.GetMailAddress; //Viser emailadressen på brugeren der er logget ind (nederste vestre hjørne)

            //BitmapImage avatar_image = new BitmapImage(new Uri("Assets/Avatars/" + UserData.Mailbox.UserData[0].avatar_name + ".png", UriKind.Relative));

            var avatar_image = new Uri(@"Assets/Avatars/" + Core.CoreUserMBdata.AvatarName + ".png", UriKind.Relative);

            avatar_png.ImageSource = new BitmapImage(avatar_image); //Picture properties: Don't forget to set Build Action to "Content", and Copy to output directory to "Always". 
        }

     

        private void ListBoxItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) //Gør følgende når man trykker på et af inbox card'ne
        {
            
            if (InboxCard_ListView.Items.Count > 0)
            {

               
                if (InboxCard_ListView.SelectedItem == null)
                {
                    return;
                }
                
                InboxModel media = (InboxModel)InboxCard_ListView.SelectedItem; //Giver adgang til dataen forbundet med den pågældende inbox card
     
                MimeMessage fetchmessage;

                if (Selected_folder == 1)
                {
                    Mail_ID = media.Mail_ID; //For ID'et på det pågældende inbox card

                    //checks if the method throws an exception
                    try
                    {
                        fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetInboxPath, Mail_ID.ToString());

                        mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                        mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen

                        Subject_mail.Content = fetchmessage.Subject;
                        DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                        NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                        if (media.IsRead == false) //If email is unread, then mark as read when cliked on
                        {
                            media.IsRead = true; 
                            Core.ChangeFilenameFlagToIsRead(Core.CoreUserMBdata.LocalPaths.GetInboxPath, media.Mail_ID.ToString());
                            try
                            {
                                Core.SetSeenFlagOnMailServer(Selected_folder, media.Mail_ID);
                            }
                            catch (CoreExceptions.ConnectionException)
                            {
                                return;
                            }
                           
                        }

                        //Checks if an HtmlBody Exists
                        if (fetchmessage.HtmlBody == null)
                        {
                            cefBrowser.LoadHtml(fetchmessage.TextBody);
                        }
                        else
                        {
                            cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                        }
                    }
                    catch (Exception ex) when (
                            ex is System.Xaml.XamlParseException ||
                            ex is IOException ||
                            ex is ArgumentNullException)
                    {
                        MessageBox.Show("The mail couldn't be viewed. Make sure it's not deleted in the filesystem");
                    }

                }

                if (Selected_folder == 2)
                {
                    Mail_ID = media.Mail_ID;

                    //checks if the method throws an exception
                    try
                    {
                        fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetSentPath, Mail_ID.ToString());

                        mail_to.Content = fetchmessage.To[0].Name + "<" + fetchmessage.To.OfType<MailboxAddress>().Single().Address.ToString() + ">";
                        mail_from.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">";

                        Subject_mail.Content = fetchmessage.Subject;
                        DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                        NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                        //Checks if an HtmlBody Exists
                        if (fetchmessage.HtmlBody == null)
                        {
                            cefBrowser.LoadHtml(fetchmessage.TextBody);
                        }
                        else
                        {
                            cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                        }
                    }
                    catch (Exception ex) when (
                            ex is System.Xaml.XamlParseException ||
                            ex is IOException ||
                            ex is ArgumentNullException)
                    {
                        MessageBox.Show("The mail couldn't be viewed. Make sure it's not deleted in the filesystem");
                    }
                }

                if (Selected_folder == 3)
                {
                    Mail_ID = media.Mail_ID;
                    //checks if the method throws an exception
                    try
                    {
                        fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetSpamPath, Mail_ID.ToString());

                        mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                        mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen

                        Subject_mail.Content = fetchmessage.Subject;
                        DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                        NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                        if (media.IsRead == false) //If email is unread, then mark as read when cliked on
                        {
                            media.IsRead = true;

                            Core.ChangeFilenameFlagToIsRead(Core.CoreUserMBdata.LocalPaths.GetSpamPath, media.Mail_ID.ToString());
                            try
                            {
                                Core.SetSeenFlagOnMailServer(Selected_folder, media.Mail_ID);
                            }
                            catch (CoreExceptions.ConnectionException)
                            {
                                return;
                            }

                           
                            
                        }

                        //Checks if an HtmlBody Exists
                        if (fetchmessage.HtmlBody == null)
                        {
                            cefBrowser.LoadHtml(fetchmessage.TextBody);
                        }
                        else
                        {
                            cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                        }
                    }
                    catch (Exception ex) when (
                            ex is System.Xaml.XamlParseException ||
                            ex is IOException ||
                            ex is ArgumentNullException)
                    {
                        MessageBox.Show("The mail couldn't be viewed. Make sure it's not deleted in the filesystem");
                    }

                }

                if (Selected_folder == 4)
                {
                    Mail_ID = media.Mail_ID;
                    //checks if the method throws an exception
                    try
                    {
                        fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetTrashPath, Mail_ID.ToString());

                        mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                        mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen

                        Subject_mail.Content = fetchmessage.Subject;
                        DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                        NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                        if (media.IsRead == false) //If email is unread, then mark as read when cliked on
                        {
                            media.IsRead = true;
                           
                            Core.ChangeFilenameFlagToIsRead(Core.CoreUserMBdata.LocalPaths.GetTrashPath, media.Mail_ID.ToString());

                            try
                            {
                                Core.SetSeenFlagOnMailServer(Selected_folder, media.Mail_ID);
                            }
                            catch (CoreExceptions.ConnectionException)
                            {
                                return;
                            }

                        }

                        //Checks if an HtmlBody Exists
                        if (fetchmessage.HtmlBody == null)
                        {
                            cefBrowser.LoadHtml(fetchmessage.TextBody);
                        }
                        else
                        {
                            cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                        }
                    }
                    catch (Exception ex) when (
                            ex is System.Xaml.XamlParseException ||
                            ex is IOException ||
                            ex is ArgumentNullException)
                    {
                        MessageBox.Show("The mail couldn't be viewed. Make sure it's not deleted in the filesystem");
                    }

                    //int mail_ID = UserData.Data.Total_Emails_Spam[0] - 1 - media.Mail_ID; //For ID'et på det pågældende inbox card
                    //mail_from.Content = Core.CoreMailbox.Spamdata[mail_ID].Mail_From; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                    //mail_to.Content = Core.CoreMailbox.Spamdata[mail_ID].Mail_To; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen
                    //cefBrowser.LoadHtml(Core.CoreMailbox.Spamdata[mail_ID].HTML_Body, true); //Viser den pågældende mail i cefBrowseren
                }

            }
        }


        #region ContextMenu Methods

        private void ListBoxItem_MouseRightButtonUp(object sender, MouseEventArgs e)
        {
            //Opening contextmenu
            ContextMenu contextMenu = InboxCard_ListView.TryFindResource("ListViewContextMenu") as ContextMenu;
            contextMenu.IsOpen = true;
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            Core.REFRESH();
        }

        private void ReadOrUnread_OnClick(object sender, RoutedEventArgs e)
        {
            InboxModel media = (InboxModel)InboxCard_ListView.SelectedItems[0];

            if (media.IsRead == false) //If email is unread, then mark as read when cliked on
            {
                media.IsRead = true;
                Core.ChangeFilenameFlagToIsRead(Core.CoreUserMBdata.LocalPaths.GetInboxPath, media.Mail_ID.ToString());
                
                try
                {
                    Core.SetSeenFlagOnMailServer(Selected_folder, media.Mail_ID);
                }
                catch (CoreExceptions.ConnectionException)
                {
                    return;
                }

            }
            else
            {
                media.IsRead = false;
                Core.ChangeFilenameFlagToUnRead(Core.CoreUserMBdata.LocalPaths.GetInboxPath, media.Mail_ID.ToString());

                try
                {
                    Core.SetUnSeenFlagOnMailServer(Selected_folder, media.Mail_ID);
                }
                catch(CoreExceptions.ConnectionException)
                {
                    return;
                }
            }

        }

        private void MoveToTrash_OnClick(object sender, RoutedEventArgs e)
        {
            InboxModel media = (InboxModel)InboxCard_ListView.SelectedItems[0];

            Properties.Settings DynPath = Properties.Settings.Default;

            string TmpPath;

            switch (_selected_folder)
            {
                case 1:
                    TmpPath = DynPath.InboxPath;
                    try
                    {
                        Core.MoveToTrash(TmpPath, media.Mail_ID.ToString());
                        Core.REFRESH();
                    }
                    catch (CoreExceptions.ConnectionException)
                    {
                        MessageBox.Show("This Action Requires Internet"); 
                    }
                    break;
                case 2:
                    TmpPath = DynPath.SentPath;
                    try
                    {
                        Core.MoveToTrash(TmpPath, media.Mail_ID.ToString());
                        Core.REFRESH();
                    }
                    catch (CoreExceptions.ConnectionException)
                    {
                        MessageBox.Show("This Action Requires Internet");
                    }
                    break;
                case 3:
                    TmpPath = DynPath.SpamPath;

                    try
                    {
                        Core.MoveToTrash(TmpPath, media.Mail_ID.ToString());
                        Core.REFRESH();
                    }
                    catch (CoreExceptions.ConnectionException)
                    {
                        MessageBox.Show("This Action Requires Internet");
                    }
                    break;
                case 4:
                    TmpPath = DynPath.TrashPath;

                    try
                    {
                        Core.MoveToTrash(TmpPath, media.Mail_ID.ToString());
                        Core.REFRESH();
                    }
                    catch (CoreExceptions.ConnectionException)
                    {
                        MessageBox.Show("This Action Requires Internet");
                    }
                    break;

                default:
                    break;
            }
        }


        private void Delete_OnClick(object sender, RoutedEventArgs e)
        {
            InboxModel media = (InboxModel)InboxCard_ListView.SelectedItems[0];

            try { 
                Core.CompletelyDeleteMail(Selected_folder, media.Mail_ID);
            }
            catch (CoreExceptions.ConnectionException)
            {
                MessageBoxResult result = MessageBox.Show("Deletion will only occur locally and the deleted file might be downloaded  when syncronizing or restarting the program", "Connection to server failed", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch(result)
                {
                    case MessageBoxResult.Yes:
                        Properties.Settings dynpaths = Properties.Settings.Default;

                        if (Selected_folder == 1)
                        {
                            var filepath = dynpaths.InboxPath;
                            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                            FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(media.Mail_ID.ToString() + "*");

                            var t = filesInDir;

                            File.Delete(filesInDir[0].FullName);

                        }
                        else if (Selected_folder == 2)
                        {
                            var filepath = dynpaths.SentPath;
                            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                            FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(media.Mail_ID.ToString() + "*");

                            var t = filesInDir;

                            File.Delete(filesInDir[0].FullName);

                        }
                        else if (Selected_folder == 3)
                        {
                            var filepath = dynpaths.SpamPath;
                            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                            FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(media.Mail_ID.ToString() + "*");

                            var t = filesInDir;

                            File.Delete(filesInDir[0].FullName);


                        }
                        else if (Selected_folder == 4)
                        {
                            var filepath = dynpaths.TrashPath;
                            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(filepath);
                            FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(media.Mail_ID.ToString() + "*");

                            var t = filesInDir;

                            File.Delete(filesInDir[0].FullName);
                        }

                        break;

                    case MessageBoxResult.No:
                        return;
                       
                }
            }

        }

        #endregion


        #region folderlist 

        private void rdInbox_Click(object sender, RoutedEventArgs e)  //Gør følgende når man trykker på "Inbox" menuen til venstre
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                ListView_Name.Content = "Inbox";
            }
            else
            {
                ListView_Name.Content = "Inbox";
            }

            _selected_folder = 1;

            if (Core.CoreMailbox.Inboxdata != null && Core.CoreMailbox.Inboxdata.Count > 0)
            {
                cefBrowser_Winforms.Visibility = Visibility.Visible;
                int index = Core.CoreMailbox.Inboxdata.Count() - 1;
                string mailid = Core.CoreMailbox.Inboxdata[index].Mail_ID.ToString();
                var fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetInboxPath, mailid);

                NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen


                Subject_mail.Content = fetchmessage.Subject;
                DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                if (fetchmessage.HtmlBody == null)
                {
                    cefBrowser.LoadHtml(fetchmessage.TextBody);
                }
                else
                {
                    cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                }

                _collection.Clear();
                for (int i = Core.CoreMailbox.Inboxdata.Count - 1; 0 < i; --i)
                {
                    if (Core.CoreMailbox.Inboxdata[i].Mail_Date_Time != null)
                    {
                        _collection.Add(new InboxModel
                        {
                            FromUsername = Core.CoreMailbox.Inboxdata[i].Mail_From.Truncate_func(35),
                            Subject = Core.CoreMailbox.Inboxdata[i].Mail_Subject,
                            Date_Time = Core.CoreMailbox.Inboxdata[i].Mail_Date_Time,
                            Mail_ID = Core.CoreMailbox.Inboxdata[i].Mail_ID,
                            IsRead = Core.CoreMailbox.Inboxdata[i].IsRead
                        });
                    }
                }
            }
            else
            {
                _collection.Clear();
                cefBrowser_Winforms.Visibility = Visibility.Hidden;
            }

           
        }


        private void rdSent_Click(object sender, RoutedEventArgs e)
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                ListView_Name.Content = "Gesendet";
            }
            else
            {
                ListView_Name.Content = "Sent";
            }

            _selected_folder = 2;

            if (Core.CoreMailbox.Sentdata != null && Core.CoreMailbox.Sentdata.Count > 0)
            {
                cefBrowser_Winforms.Visibility = Visibility.Visible;
                
                int index = Core.CoreMailbox.Sentdata.Count() - 1;
                string mailid = Core.CoreMailbox.Sentdata[index].Mail_ID.ToString();
                var fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetSentPath, mailid);

                NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen


                Subject_mail.Content = fetchmessage.Subject;
                DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                if (fetchmessage.HtmlBody == null)
                {
                    cefBrowser.LoadHtml(fetchmessage.TextBody);
                }
                else
                {
                    cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                }

                _collection.Clear();
                for (int i = Core.CoreMailbox.Sentdata.Count - 1; 0 < i; --i)
                {
                    _collection.Add(new InboxModel
                    {
                        FromUsername = Core.CoreMailbox.Sentdata[i].Mail_From.Truncate_func(35),
                        Subject = Core.CoreMailbox.Sentdata[i].Mail_Subject,
                        Date_Time = Core.CoreMailbox.Sentdata[i].Mail_Date_Time,
                        Mail_ID = Core.CoreMailbox.Sentdata[i].Mail_ID,
                        IsRead = true
                    });
                }

            }
            else
            {
                _collection.Clear();
                cefBrowser_Winforms.Visibility = Visibility.Hidden;
            }

            
        }


        private void rdSpam_Click(object sender, RoutedEventArgs e)
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                ListView_Name.Content = "Spam";
            }
            else
            {
                ListView_Name.Content = "Spam";
            }

            _selected_folder = 3;

            if (Core.CoreMailbox.Spamdata != null && Core.CoreMailbox.Spamdata.Count > 0)
            {
                cefBrowser_Winforms.Visibility = Visibility.Visible;
                
                int index = Core.CoreMailbox.Spamdata.Count() - 1;
                string mailid = Core.CoreMailbox.Spamdata[index].Mail_ID.ToString();
                var fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetSpamPath, mailid);

                NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen


                Subject_mail.Content = fetchmessage.Subject;
                DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");



                if (fetchmessage.HtmlBody == null)
                {
                    cefBrowser.LoadHtml(fetchmessage.TextBody, true);
                }
                else
                {
                    cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                }

                _collection.Clear();
                if (Core.CoreMailbox.Spamdata != null)
                {
                    for (int i = Core.CoreMailbox.Spamdata.Count - 1; 0 <= i; --i)
                    {
                        _collection.Add(new InboxModel
                        {
                            FromUsername = Core.CoreMailbox.Spamdata[i].Mail_From.Truncate_func(35),
                            Subject = Core.CoreMailbox.Spamdata[i].Mail_Subject,
                            Date_Time = Core.CoreMailbox.Spamdata[i].Mail_Date_Time,
                            Mail_ID = Core.CoreMailbox.Spamdata[i].Mail_ID,
                            IsRead = Core.CoreMailbox.Spamdata[i].IsRead
                        });
                    }
                }
            }
            else
            {
                _collection.Clear();
                cefBrowser_Winforms.Visibility = Visibility.Hidden;
            }

            
        }


        private void rdTrash_Click(object sender, RoutedEventArgs e)
        {
            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                ListView_Name.Content = "Mülleimer";
            }
            else
            {
                ListView_Name.Content = "Trash";
            }

            _selected_folder = 4;

            if (Core.CoreMailbox.Trashdata != null && Core.CoreMailbox.Trashdata.Count > 0)
            {
                cefBrowser_Winforms.Visibility = Visibility.Visible;

                int index = Core.CoreMailbox.Trashdata.Count() - 1;
                string mailid = Core.CoreMailbox.Trashdata[index].Mail_ID.ToString();
                var fetchmessage = Core.FullMailLoad(Core.CoreUserMBdata.LocalPaths.GetTrashPath, mailid);

                NewPopup.Email_textbox_text.Text = fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString();

                mail_from.Content = fetchmessage.From[0].Name + "<" + fetchmessage.From.OfType<MailboxAddress>().Single().Address.ToString() + ">"; //Viser hvem der har sent mailen og bliver vist øverst over mailen
                mail_to.Content = Core.CoreUserMBdata.GetFullName + "<" + Core.CoreUserMBdata.GetMailAddress + ">"; //Viser hvem mailen er til (brugeren højst sansyndligt) og bliver vist øverst over mailen


                Subject_mail.Content = fetchmessage.Subject;
                DateTime_mail.Content = fetchmessage.Date.DateTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");

                if (fetchmessage.HtmlBody == null)
                {
                    cefBrowser.LoadHtml(fetchmessage.TextBody);
                }
                else
                {
                    cefBrowser.LoadHtml(fetchmessage.HtmlBody, true);  //Viser den pågældende mail i cefBrowseren
                }

                _collection.Clear();
                for (int i = Core.CoreMailbox.Trashdata.Count - 1; 0 <= i; --i)
                {
                    _collection.Add(new InboxModel
                    {
                        FromUsername = Core.CoreMailbox.Trashdata[i].Mail_From.Truncate_func(35),
                        Subject = Core.CoreMailbox.Trashdata[i].Mail_Subject,
                        Date_Time = Core.CoreMailbox.Trashdata[i].Mail_Date_Time,
                        Mail_ID = Core.CoreMailbox.Trashdata[i].Mail_ID,
                        IsRead = Core.CoreMailbox.Trashdata[i].IsRead
                    });
                }
            }
            else
            {
                _collection.Clear();
                cefBrowser_Winforms.Visibility = Visibility.Hidden;
            }
        }

        #endregion

        private void New_Click(object sender, RoutedEventArgs e) //Gør følgende når man trykker på den blå knap "New" 
        {
            cefBrowser_Winforms.Visibility = Visibility.Hidden; //Gør cefBrowser vinduet usynlig så det ikke flyder ovenpå send_mail UI
            NewPopup.Visibility = Visibility.Visible; //Gør send_mail UI synligt så man kan sende mails
            new_reply_forward = 1;

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                NewPopup.NewPopup_Title.Content = "Neu";
                NewPopup.Email_textbox_text.Text = "E-mail";
                NewPopup.Subject_textbox_text.Text = "Gegenstand";
                NewPopup.Body_textbox_text.Text = "Nachricht";
            }
            else
            {
                NewPopup.NewPopup_Title.Content = "New";
                NewPopup.Email_textbox_text.Text = "E-mail";
                NewPopup.Subject_textbox_text.Text = "Subject";
                NewPopup.Body_textbox_text.Text = "Message";
            }

           
            NewPopup.Email_textbox_text.Foreground = Brushes.DarkGray;
            NewPopup.Email_textbox.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e) //Knap for at få cefBrowseren med mailen frem efter at have sent. En meget midlertidig løsning, men den virker ;)
        {
            cefBrowser_Winforms.Visibility = Visibility.Visible;    //Sætter cefBrowseren til at være syndlig
        }

        private void Reply_button_Click(object sender, RoutedEventArgs e)
        {
            cefBrowser_Winforms.Visibility = Visibility.Hidden; //Gør cefBrowser vinduet usynlig så det ikke flyder ovenpå send_mail UI
            NewPopup.Visibility = Visibility.Visible; //Gør send_mail UI synligt så man kan sende mails
            new_reply_forward = 2;
            NewPopup.Email_textbox_text.Foreground = Brushes.Black;
            NewPopup.Email_textbox.Visibility = Visibility.Hidden;



            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                NewPopup.NewPopup_Title.Content = "Beantwortet";
                NewPopup.Subject_textbox_text.Text = "Gegenstand";
                NewPopup.Body_textbox_text.Text = "Nachricht";
            }
            else
            {
                NewPopup.NewPopup_Title.Content = "Reply";
                NewPopup.Subject_textbox_text.Text = "Subject";
                NewPopup.Body_textbox_text.Text = "Message";
            }

        }

        private void Forward_button_Click(object sender, RoutedEventArgs e)
        {
            cefBrowser_Winforms.Visibility = Visibility.Hidden; //Gør cefBrowser vinduet usynlig så det ikke flyder ovenpå send_mail UI
            NewPopup.Visibility = Visibility.Visible; //Gør send_mail UI synligt så man kan sende mails
            new_reply_forward = 3;

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                NewPopup.NewPopup_Title.Content = "Weiter leiten";
                NewPopup.Email_textbox_text.Text = "E-mail";
                NewPopup.Subject_textbox_text.Text = "Gegenstand";
                NewPopup.Body_textbox_text.Text = "Nachricht";
            }
            else
            {
                NewPopup.NewPopup_Title.Content = "Forward";
                NewPopup.Email_textbox_text.Text = "E-mail";
                NewPopup.Subject_textbox_text.Text = "Subject";
                NewPopup.Body_textbox_text.Text = "Message";
            }

            
            NewPopup.Email_textbox_text.Foreground = Brushes.DarkGray;
            NewPopup.Email_textbox.Visibility = Visibility.Visible;
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            cefBrowser_Winforms.Visibility = Visibility.Hidden;
            Settings_popup.Visibility = Visibility.Visible;
        }

        private void SwitchUserButton_Click(object sender, RoutedEventArgs e)
        {
            SwitchUser_popup.Visibility = Visibility.Visible;
            cefBrowser_Winforms.Visibility = Visibility.Hidden;
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            
            cefBrowser_Winforms.Visibility = Visibility.Hidden;
            Box_popup.Visibility = Visibility.Visible;

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                Box_popup.ListView_Name.Content = "Neu laden...";
                MessageBox.Show("Das Aktualisieren Ihrer E-Mail-Ordner kann einige Minuten dauern!");
            }
            else
            {
                Box_popup.ListView_Name.Content = "Refreshing...";
                MessageBox.Show("Refreshing your mail folders can take a few minutes!");
            }


            Core.REFRESH();

           
            if (Core.Flags.Refresing == false)
            {
                rdInbox.DataContext = Core.CoreUserMBdata.GetInboxUnreadCount.ToString(); //Number of unread eamil displayed in UI
                Box_popup.Visibility = Visibility.Hidden;
                cefBrowser_Winforms.Visibility = Visibility.Visible;
                if (Selected_folder == 1)
                {
                    rdInbox_Click(sender, e);
                }
                if (Selected_folder == 2)
                {
                    rdSent_Click(sender, e);
                }
                if (Selected_folder == 3)
                {
                    rdSpam_Click(sender, e);
                }
                if (Selected_folder == 4)
                {
                    rdTrash_Click(sender, e);
                }
            }
            

        }

        private void Sync_Click(object sender, RoutedEventArgs e)
        {
            cefBrowser_Winforms.Visibility = Visibility.Hidden;
            Box_popup.Visibility = Visibility.Visible;

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                Box_popup.ListView_Name.Content = "Synchronisieren...";
                MessageBox.Show("Synchronisieren Ihrer E-Mail-Ordner kann einige Minuten dauern!");
            }
            else
            {
                Box_popup.ListView_Name.Content = "Synchronizing...";
                MessageBox.Show("Synchronizing your mail folders can take a few minutes!");
            }

            try { 
                Core.SYNCRONIZE();
                rdInbox.DataContext = Core.CoreUserMBdata.GetInboxUnreadCount.ToString(); //Number of unread eamil displayed in UI
                Box_popup.Visibility = Visibility.Hidden;
                cefBrowser_Winforms.Visibility = Visibility.Visible;

                if (Selected_folder == 1)
                {
                    rdInbox_Click(sender, e);
                }
                if (Selected_folder == 2)
                {
                    rdSent_Click(sender, e);
                }
                if (Selected_folder == 3)
                {
                    rdSpam_Click(sender, e);
                }
                if (Selected_folder == 4)
                {
                    rdTrash_Click(sender, e);
                }

            }
            catch(Exception ex) when (ex is System.Net.Sockets.SocketException || ex is AuthenticationException)
            {
                MessageBox.Show("Currently Offline");
            }
        }
    }
}
