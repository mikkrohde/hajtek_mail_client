﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hajtek_mail_client_dev
{
    /// <summary>
    /// Interaction logic for SetupDone.xaml
    /// </summary>
    public partial class SetupDone : UserControl
    {
        public SetupDone()
        {
            InitializeComponent();

        }

        private void Close_NewPopup_Click(object sender, RoutedEventArgs e)
        {
            Core.RESTARTAPP();
        }
    }
}
