﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;
using System.ComponentModel;

namespace Hajtek_mail_client_dev.Model
{
    public class InboxModel : INotifyPropertyChanged
    {
        public string FromUsername { get; set; }
        public string FromUsername_Short { get; set; }
        public string ImageSource { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DayOfWeek Time { get; set; }
        public string Date_Time { get; set; }
        public int Mail_ID { get; set; }

        //public bool IsRead { get; set; }

        private bool _isRead = false;
        public bool IsRead
        {
            get { return _isRead; }
            set
            {
                _isRead = value;
                NotifyPropertyChanged(); // Anytime this property is changed Property Changed will be executed
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}