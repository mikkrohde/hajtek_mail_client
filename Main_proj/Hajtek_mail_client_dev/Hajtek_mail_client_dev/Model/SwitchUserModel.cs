﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev;
using Hajtek_mail_client_dev.data_structures.email_data_structure;

namespace Hajtek_mail_client_dev.Model
{
    public class SwitchUserModel
    {
        public string FullName      { get; set; }
        public string EmailAddress  { get; set; }
        public string AccUsername   { get; set; }
        public Uri AvatarName       { get; set; }

    }
}