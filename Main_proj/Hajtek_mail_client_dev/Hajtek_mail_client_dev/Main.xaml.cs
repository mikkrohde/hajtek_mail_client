﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Hajtek_mail_client_dev.libraries.setup_lib;
using Hajtek_mail_client_dev.Model;
using CefSharp;
using CefSharp.Web;
using CefSharp.Internals;
using CefSharp.WinForms;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;
using Hajtek_mail_client_dev.libraries.ScrollViewer;
using System.Collections.ObjectModel;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using System.IO;
using System.Xml.Linq;
using System.ComponentModel;

namespace Hajtek_mail_client_dev
{
    //Dette er det første vindue der åbner når du kører programmet (kald det for main)
    //Det tjekker om der allerede findes en XML fil med oplysninger i mappen XML
    //Hvis ikke så åbner den LogIn_Popup, hvor man kan indtaste sine oplysninger
    //Hvis der derimod findes en XML fil, så åbner den MainWindow_Popop, som er vores mail client

    //Indtil videre kan kun en bruger oprettes


    /* Hajtek Mail Client execution route 
     * 
     * 1. public Main
     *      => InitializeComponent();
     *      
     *      ↓
     *      
     * 2. public MainWindow
     *      => Initialize component()
     *      - if var. FirstLaunch is equal to false
     *          => Core.Startup()
     *      => setDefaultEncoding()
     *      => RunDefault();
     *      
     *          
     * 
     */


    public partial class Main : Window
    {
        public static bool _use_german;
        
        /*
        public Visibility Pick_avatar_Popup_Visibility_property
        {
            get { return Hajtek_mail_client_dev.LogIn._pick_avatar_Popup_Visibility_property; }
            
        }
        */
        

        public string First_name
        {
            get { return Hajtek_mail_client_dev.LogIn._first_name; }
        }

        public Main()
        {
            

            InitializeComponent();

            
           

            pick_avatar_popup.Visibility = Visibility.Hidden;
           

            Loaded += MainPage_Loaded;


            if (Properties.Settings.Default.FirstLaunch == false)
            {
                MainWindow_Popup.Visibility = Visibility.Visible;
            }

            

        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var text = new Welcome_text_class();
            DataContext = text;

            while (true)
            {
                text.Welcome_text = "Welcome to your new mail client";
                text.Welcome_text_small = "Please select a language";
                await Task.Delay(3000);
                text.Welcome_text = "Willkommen in Ihre neue Mail client";
                text.Welcome_text_small = "Bitte wähle eine Sprache";
                await Task.Delay(3000);
            }
        }

        public class Welcome_text_class : INotifyPropertyChanged
        {
            private string _welcome_text;
            private string _welcome_text_small;
            public string Welcome_text
            {
                get => _welcome_text; set
                {
                    _welcome_text = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Welcome_text)));
                }
            }
            public string Welcome_text_small
            {
                get => _welcome_text_small; set
                {
                    _welcome_text_small = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Welcome_text_small)));
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
        }

        //Her mangler det at når man har oprettet sig som bruger så skal man lukke programmet og åbne det igen, ikke godt nok

        //Da dette er hovedvinduet, så står den også for at minimerer, maximerer og lukke helt for hele programmet
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //ensures changed UserData.UserInfo data is saved in XML file.
            Core.SAVE();
            

            Close(); //Lukker hele programmet
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal) //Hvis vinduet er "normal" så maximerer den det
                WindowState = WindowState.Maximized;
            else
                WindowState = WindowState.Normal; //Hvis det allerede er maximeret så gør den det "normalt" igen
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized; //Mninmerer vinduet
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove(); //Gør det muligt at flytter hele vinduet ved at trække i hitboxen (toppen af programmet, cirka 15 pixel)
            }
        }

        private void german_selected_Click(object sender, RoutedEventArgs e)
        {
            _use_german = true;
            Core.FirstTimeSetup();
            pick_avatar pick_Avatar = new pick_avatar();
            LogIn_Popup.Visibility = Visibility.Visible;
            pick_avatar_popup.Visibility = Visibility.Visible;
            SetupDone_popup.Visibility = Visibility.Visible;

            LogIn_Popup.First_name_textbox_backtext.Text = "Vorname";
            LogIn_Popup.Last_name_textbox_backtext.Text = "Nachname";
            LogIn_Popup.mail_address_textbox_backtext.Text = "E-Mail-Addresse";
            LogIn_Popup.Account_name_textbox_backtext.Text = "Benutzername";
            LogIn_Popup.Account_password_textbox_backtext.Text = "Passwort";
            LogIn_Popup.SMTP_host_textbox_backtext.Text = "SMTP Gastgeber";
            LogIn_Popup.SMTP_port_textbox_backtext.Text = "SMTP Anschlussstelle";
            LogIn_Popup.USE_SSL_SMTP_button.Content = "SSL für SMTP verwenden?";
            LogIn_Popup.IMAP_host_textbox_backtext.Text = "IMAP Gastgeber";
            LogIn_Popup.IMAP_port_textbox_backtext.Text = "IMAP Anschlussstelle";
            LogIn_Popup.USE_SSL_IMAP_button.Content = "SSL für IMAP verwenden?";
            LogIn_Popup.Login_Hint.Text = "Hinweis: Verwenden Sie für post.au.dk-Mail den SMTP-Port 465, den IMAP-Port 993 und verwenden Sie SSL für SMTP und IMAP";

            pick_avatar_popup.Wellcome_text.Text = "Willkommen bei Ihrer neuer Mail-Client";
            pick_avatar_popup.Wellcome_text_small.Text = "Bitte wähle eine Avatar der zu ihnen passt";

            SetupDone_popup.Done_text.Text = "Vielen Dank für die Einrichtung von Hajtek Mail Client";
            SetupDone_popup.Done_text_small.Text = "Wenn Sie auf die Taste klicken, speichern Sie den gerade eingerichteten Benutzer und das Programm muss mit diesem neuen Benutzer gestartet werden";


        }

        private void english_selected_Click(object sender, RoutedEventArgs e)
        {
            _use_german = false;
            Core.FirstTimeSetup();
            LogIn_Popup.Visibility = Visibility.Visible;
            pick_avatar_popup.Visibility = Visibility.Visible;
            SetupDone_popup.Visibility = Visibility.Visible;

            LogIn_Popup.First_name_textbox_backtext.Text = "First Name";
            LogIn_Popup.Last_name_textbox_backtext.Text = "Last Name";
            LogIn_Popup.mail_address_textbox_backtext.Text = "Email address";
            LogIn_Popup.Account_name_textbox_backtext.Text = "Account name";
            LogIn_Popup.Account_password_textbox_backtext.Text = "Account password";
            LogIn_Popup.SMTP_host_textbox_backtext.Text = "SMTP Host";
            LogIn_Popup.SMTP_port_textbox_backtext.Text = "SMTP Port";
            LogIn_Popup.USE_SSL_SMTP_button.Content = "Use SSL for SMTP?";
            LogIn_Popup.IMAP_host_textbox_backtext.Text = "IMAP Host";
            LogIn_Popup.IMAP_port_textbox_backtext.Text = "IMAP Port";
            LogIn_Popup.USE_SSL_IMAP_button.Content = "Use SSL for IMAP?";
            LogIn_Popup.Login_Hint.Text = "Hint:  For post.au.dk mail, use SMTP port 465, IMAP port 993 and use SSL for SMTP and IMAP";

            pick_avatar_popup.Wellcome_text.Text = "Welcome to your new mail client";
            pick_avatar_popup.Wellcome_text_small.Text = "Please select an avatar that fits you";

            SetupDone_popup.Done_text.Text = "Thanks for setting up Hajtek Mail Client";
            SetupDone_popup.Done_text_small.Text = "By clicking on the button below, you save the user you just setup and the program restart";


        }
    }
}
