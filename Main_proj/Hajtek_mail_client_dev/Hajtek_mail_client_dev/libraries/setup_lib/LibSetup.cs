﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Xml;
using System.Xml.Serialization;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using System.IO;
using System.Xml.Linq;

namespace Hajtek_mail_client_dev.libraries.setup_lib
{

    public abstract class Lib_setup
    {
        //public abstract methods
        public abstract UserData.UserInfo Init();

        //protected abstract methods
        protected abstract bool TestConToServer(UserData.UserInfo Login_Info, bool ImapSSL);

        /// <summary>
        /// Checks if a mailbox file exists
        /// </summary>
        /// <return>bool</return>
        protected bool MailboxFileExists(string filename)
        {
            if (!File.Exists(GetXMLPath(filename)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Gets path of XML files 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected string GetXMLPath(string filename)
        {
            string mainpath = Path.Combine(Application.StartupPath, "PROFILES");
            string fullpath = Path.Combine(mainpath, filename + ".xml");
            //string path = Path.Combine(Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "XML"), filename + ".xml");
            return fullpath;
        }


        #region Read Write XML data

        /// <summary>
        /// Creates a textwriter, for creation of XML. 
        /// <para>Current implementation only supports debug</para>
        /// <para>Rewrite for Release</para>
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected TextWriter CreateMailboxDataFile(string filename)
        {
            string mainpath = Path.Combine(Application.StartupPath, "PROFILES");
            string path = Path.Combine(mainpath, filename + ".xml");
            TextWriter writer = new StreamWriter(path);
            return writer;
        }

        /// <summary>
        /// Creates a serializer with type of UserData.Userinfo
        /// </summary>
        /// <returns></returns>
        protected XmlSerializer CreateSerializerXML() {
            XmlSerializer serializer = new XmlSerializer(typeof(UserData.UserInfo));
            return serializer;
        }


        /// <summary>
        /// Writes all the userdata of type UserData.Userinfo to an XML file.
        /// The binding elements are a list of the XAML elements. Used in following order
        /// <list type="number">
        /// <item><param name="T">First name</param></item>
        /// <item><param name="T">Last name</param></item>
        /// <item><param name="T">Mail Address</param></item>
        /// <item><param name="T">Account username</param></item>
        /// <item><param name="T">Account password</param></item>
        /// <item><param name="T">SMTP host</param></item>
        /// <item><param name="T">IMAP host</param></item>
        /// <item><param name="T">SMTP port</param></item>
        /// <item><param name="T">IMAP port</param></item>
        /// <item><param name="T">SMTP SSL</param></item>
        /// <item><param name="T">IMAP SSL</param></item>
        /// <item><param name="T">Current Mail Count</param></item>
        /// </list>
        /// </summary>
        /// <param name="UserAdded"></param>
        /// <param name="Imapssl"></param>
        /// <param name="Smtpssl"></param>
        /// <param name="BindingElements"></param>
        protected UserData.UserInfo WriteUserDataToXML(UserData.UserInfo UserAdded, bool ImapSSL, bool SmtpSSL, bool UseGerman, params string[] BindingElements)
        {
            // Creates an instance of the XmlSerializer class;
            // specifies the type of object to serialize.

            UserAdded.FirstName = BindingElements[0];
            UserAdded.LastName = BindingElements[1];

            string filename = UserAdded.FirstName + UserAdded.LastName; //Filename? Mener du ikke fullname?

            XmlSerializer newSerializer = CreateSerializerXML();
            TextWriter newUserFile = CreateMailboxDataFile(filename);

            UserData.UserInfo tmp = new UserData.UserInfo();

            tmp.FirstName = BindingElements[0];
            tmp.LastName = BindingElements[1];
            tmp.FullName = BindingElements[0] + " " + BindingElements[1];
            tmp.MailAddress = BindingElements[2];
            tmp.AccountUsername = BindingElements[3];
            tmp.AccountPassword = BindingElements[4];
            tmp.Smtp_Host = BindingElements[5];
            tmp.Imap_Host = BindingElements[6];
            tmp.Smtp_PORT = int.Parse(BindingElements[7]);
            tmp.Imap_PORT = int.Parse(BindingElements[8]);
            tmp.Use_ssl_IMAP = ImapSSL;
            tmp.Use_ssl_SMTP = SmtpSSL;
            tmp.CurrMailCount = 0;
            tmp.CurrMailUnReadCount = 0;
            tmp.CurrSentCount = 0;
            tmp.CurrDraftCount = 0;
            tmp.CurrArchiveCount = 0;
            tmp.CurrTrashCount = 0;
            tmp.AvatarName = BindingElements[9];
            tmp.Use_german = UseGerman;

            // Serializes
            newSerializer.Serialize(newUserFile, tmp);
            newUserFile.Close();

            return tmp;
        }

        /// <summary>
        /// Update a xml file using XmlDocument class
        /// Changes the inner text of node element.
        /// - Choose what nodes to edit.
        /// </summary>
        /// <param name="UserToUpdate"></param>
        /// <param name="NodeToEdit">Case Sensitive</param>
        /// <param name="NewValue">Case Sensitive</param>
        protected void updateXML(UserData.UserInfo UserToUpdate, string NodeToEdit, string NewValue)
        {
    
            string path = GetXMLPath(UserToUpdate.FirstName + UserToUpdate.LastName);
        
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                xmldoc.Load(path);
                xmldoc.SelectSingleNode("UserInfo" + "/" + NodeToEdit).InnerText = NewValue;
                xmldoc.Save(path);
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                Console.WriteLine(exc.Data);
            }
        }


        /// <summary>
        /// Need a reimplementation such that it isnt static elements are used.
        /// This requires different solution than struct data.
        /// </summary>
        /// <param name="User"></param>
        /// <param name="input"></param>
        protected void AddAvatarToXML(string input)
        {
            string path = Path.Combine(Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "XML"), "XML_test.xml");

            XDocument doc = XDocument.Load(path);

            UserData.Data.Avatar_name.Add(input);

            string temp_string;
            UserData.Data.Avatar_name.TryPeek(out temp_string);
            doc.Element("Mailbox_data_struct").Element("avatar_name").Value = temp_string;

            doc.Save(path);
        }

        /// <summary>
        /// Fetches Specific User from xml file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>UserData.UserInfo</returns>
        protected UserData.UserInfo ReadFromMBDataFile(string filename)
        {
            UserData.UserInfo tmp;
            string path = GetXMLPath(filename);
            using(FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer newSerializer = CreateSerializerXML();
                tmp = (UserData.UserInfo)newSerializer.Deserialize(fs);
            }
            return tmp;
        }


        /*public void read_XML_func(string filename) //Funktionen til at læse XML filen (nem at have herinde da det er her den skal bruges)
        {
            string path = Path.Combine(Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "XML"), filename);
            XmlSerializer serializer = new XmlSerializer(typeof(User_Info.Mailbox_data_struct));
            FileStream fs = new FileStream(path, FileMode.Open);
            User_Info.Mailbox_data_struct e;
            e = (User_Info.Mailbox_data_struct)serializer.Deserialize(fs);
            User_Info.Mailbox_data_class.UserData.Add(e);
        }*/

        #endregion


    }
}