﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using MailKit.Net;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace Hajtek_mail_client_dev.libraries.setup_lib
{
    class Imap_setup : Lib_setup, IDisposable
    {
        private bool disposedValue;

        /// <summary>
        /// Gets the userdata from the user as arguments and store them in XML file
        /// Default values:
        ///     - ImapSSL: true
        ///     - SmtpSSL: true
        /// </summary>
        /// <returns>void</returns>
        public void NewUser(UserData.UserInfo _user, bool _ImapSSL = true, bool _SmtpSSL = true, bool _UseGerman = false, params string[] _GUIelements)
        {
            DirMgmtLocal dir = new DirMgmtLocal();
            //maybe add checks to ensure that the necessary data is given before running NewUserData
            var newuser = WriteUserDataToXML(_user, _ImapSSL, _SmtpSSL, _UseGerman, _GUIelements);

            //  Change the Current Active User to the newly created user
            //  Stores the fullname in App.settings ActiveUser variable.
            Properties.Settings.Default.ActiveUser = _GUIelements[0] + _GUIelements[1];
            Properties.Settings.Default.Save();

            //Creates Mailbox folders for new user.
            string MailBoxPath = Path.Combine("USERS", newuser.AccountUsername);
            dir.CreateSubfolder("USERS", newuser.AccountUsername);
            dir.CreateSubfolder(MailBoxPath, "Inbox");
            dir.CreateSubfolder(MailBoxPath, "Sent");
            dir.CreateSubfolder(MailBoxPath, "Draft");
            dir.CreateSubfolder(MailBoxPath, "Spam");
            dir.CreateSubfolder(MailBoxPath, "Archive");
            dir.CreateSubfolder(MailBoxPath, "Trash");
        }

        private UserData.UserInfo LoadPrevActiveUser()
        {
            /*
             * Inside project settings we can set variables which is stored next time the project opens
             * Using that we can dynamicallly store which user was last logged in, 
             * such that it can easier support multiuser login 
             */
            string activeuser = Properties.Settings.Default.ActiveUser;
            var LoadedUser = ReadFromMBDataFile(activeuser);
            return LoadedUser;
        }

        public UserData.UserInfo LoadNewActiveUser(string UserName)
        {
            /*
             * Loading a new user in runtime
             * 1. Access Profile folder
             * 2. Get the file with the specific filename
             */
            UserData.UserInfo tmp;
            tmp = ReadFromMBDataFile(UserName);
            return tmp;
        }
       
        /// <summary>
        /// Initializer for entire mail software.
        /// - pulls last active user in the software
        /// </summary>
        /// <returns>Mailbox_data</returns>
        public override UserData.UserInfo Init()
        {
            //gets the userinfo from the last active user.
            UserData.UserInfo ActiveUser = LoadPrevActiveUser();
            //returns the active user 
            return ActiveUser;
        }

        public void UpdateXMLInnerText(UserData.UserInfo UpdateUser, string EditNode, string NewValue)
        {
            updateXML(UpdateUser, EditNode, NewValue);
        }
   
        /// <summary>
        /// connects to the server based on the data given.
        /// - Creates connection to the imap mailbox and tests if works
        /// </summary>
        /// <returns>Mailbox_data</returns>
        protected override bool TestConToServer(UserData.UserInfo Login_Info, bool ImapSSL){
            using (var Client_Test = new ImapClient())
            {
                Client_Test.Connect(Login_Info.Imap_Host, Login_Info.Imap_PORT, ImapSSL);
                Client_Test.Authenticate(Login_Info.AccountUsername, Login_Info.AccountPassword);

                if(!Client_Test.IsConnected)
                {
                    return false;
                }
                else
                {
                    Client_Test.Disconnect(true);
                }
            }
            return true;
        }


        /// <summary>
        /// Completely identical to the one above....
        /// This method is used for debug purposes: delete when it becomes unnecessary
        /// </summary>
        /// <returns>Mailbox_data</returns>
        public bool TestImapConnection(UserData.UserInfo Login_Info)
        {
            try { 
                using (var Client_Test = new ImapClient())
                {
                    Client_Test.Connect(Login_Info.Imap_Host, Login_Info.Imap_PORT, SecureSocketOptions.SslOnConnect);

                    try {
                        Client_Test.Authenticate(Login_Info.AccountUsername, Login_Info.AccountPassword); 
                    } catch (Exception ex) when (ex is AuthenticationException)
                    {
                        throw new AuthenticationException();
                    }

                    if (!Client_Test.IsConnected)
                    {
                        Console.WriteLine("Failed connection");
                        return false;
                    }
                    else
                    {
                        Console.WriteLine("Successful connection");
                        Client_Test.Disconnect(true);
                    }
                }
            }
            catch(System.Net.Sockets.SocketException)
            {
                throw new System.Net.Sockets.SocketException();
            }

            return true;
        }

        public bool TestSMTPConnection(UserData.UserInfo Login_Info)
        {
            using (var Client_Test = new SmtpClient())
            {
                Client_Test.Connect(Login_Info.Smtp_Host, Login_Info.Smtp_PORT, SecureSocketOptions.Auto);

                try
                {
                    Client_Test.Authenticate(Login_Info.AccountUsername, Login_Info.AccountPassword);
                }
                catch (Exception ex) when (ex is MailKit.Security.AuthenticationException)
                {
                    throw new MailKit.Security.AuthenticationException();
                }

                if (!Client_Test.IsConnected)
                {
                    Console.WriteLine("Failed connection");
                    return false;
                }
                else
                {
                    Console.WriteLine("Successful connection");
                    Client_Test.Disconnect(true);
                }
            }
            return true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Imap_setup()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
