﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hajtek_mail_client_dev.libraries.exceptions
{
    class DirLibException : Exception
    {    
    }

    
    /*
     * Exceptions pertaining to:
     * - IReceiveMail
     * - IMailOP
     * - ISendMail
     */
    public class MailLibException : Exception
    {
        public class UserArgumentNULLException : Exception {
            /// <summary>
            /// UserArgumentNULLException can be used to check if valid arguments 
            /// <para>are given to the class constructor</para>
            /// </summary>

            public UserArgumentNULLException()
            {
            }

            public UserArgumentNULLException(string message) : base(string.Format("InvalidUserArgument: {0}", message))
            {
            }

            public UserArgumentNULLException(string message, Exception inner) : base(message, inner)
            {

            }

        }

        public class InboxNULLException : Exception
        {

            /// <summary>
            /// This exception is to be thrown if the inbox folder can't be found.
            /// <para>In the path of the application: ./install-path/USERS/"specific-user"/Inbox</para>
            /// </summary>
            public InboxNULLException()
            {
            }

            public InboxNULLException(string message) : base(string.Format("InboxFolderNotFound: {0}", message))
            {
            }

            public InboxNULLException(string message, Exception inner) : base(message, inner)
            {
            }

        }



    }

    public class SetupLibException : Exception
    {

    }


    public class CoreExceptions : Exception
    {
        public class RefreshIndexexception : Exception
        {
            public RefreshIndexexception()
            {
            }

            public RefreshIndexexception(string message) : base(string.Format("InboxFolderNotFound: {0}", message))
            {
            }

            public RefreshIndexexception(string message, Exception inner) : base(message, inner)
            {

            }
        }

        public class RefreshXamlLoadexception : Exception
        {
            public RefreshXamlLoadexception()
            {
            }

            public RefreshXamlLoadexception(string message) : base(string.Format("Failed to load Mails: {0}", message))
            {
            }

            public RefreshXamlLoadexception(string message, Exception inner) : base(message, inner)
            {

            }
        }

        public class ConnectionException : Exception
        {
            public ConnectionException()
            {
            }

            public ConnectionException(string message) : base(string.Format("Failed to connected to server: {0}", message))
            {
            }

            public ConnectionException(string message, Exception inner) : base(message, inner)
            {

            }
        }


    }

}
