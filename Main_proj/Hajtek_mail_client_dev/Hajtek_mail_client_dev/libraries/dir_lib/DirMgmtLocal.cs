﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net.Imap;

namespace Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib
{
    /// <summary>
    /// <para>Class for local directory management to add, remove and find folders in the install directory during runtime</para>
    /// <para>Methods</para>
    /// <list type="bullet">
    /// <item><param name="M">void CreateSubfolder(string path, string name)</param></item>
    /// <item><param name="M">string CreateUserFolder(string mail)</param></item>
    /// <item><param name="M">string[] GetSubFolders(string folder)</param></item>
    /// <item><param name="M">void DeleteSubfolder()</param></item>
    /// </list>
    /// </summary>
    public class DirMgmtLocal : Dir_mgmt
    {

        /// <summary>
        /// Creates new subfolder in the wished directory
        /// <para><exception cref="">DirectoryNotFoundException</exception></para>
        /// <remarks>Uses typeof string as path and name</remarks>
        /// </summary>
        public override void CreateSubfolder(string path, string foldername)
        {
            string MainFolderPath = Path.Combine(Application.StartupPath, path); //Get parrent folder
            string NewFolderPath = Path.Combine(MainFolderPath, foldername);     //Create path for new folder

            //if the directory does not exist throw Directory not found exception
            if (!Directory.Exists(MainFolderPath)){
                throw new DirectoryNotFoundException();
            }
            //else, we assume it exists and create the new folder 
            else{
                Directory.CreateDirectory(NewFolderPath); //Create folder
            }
            return;
        }


        /// <summary>
        /// Create a new user folder
        /// </summary>
        /// 
        public override string CreateUserFolder(string NewUser)
        {
            string UserFolder = null;

            string NewFilePath = Path.Combine(Path.Combine(Application.StartupPath, "USERS"), NewUser);
            Directory.CreateDirectory(NewFilePath);

            string[] directoryFolders = Directory.GetDirectories(Path.Combine(Application.StartupPath, "USERS"));
            foreach(string folders in directoryFolders)
            {
                if(folders == NewUser)
                {
                    UserFolder = folders;
                }
            }
            return UserFolder;
        }

        /// <summary>
        /// Get the path of the specific users subfolders
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Path of subfolders from given folder path</returns>
        public override string[] GetSubFolders(string folder)
        {
            string[] directoryFolders = Directory.GetDirectories(Path.Combine(Application.StartupPath, folder));
            return directoryFolders;
        }

        public override void DeleteSubfolder()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        public override string AccessSingleFolder(string path, string folder) 
        {
            string folderpath = null;
            string[] Folders = Directory.GetDirectories(Path.Combine(Application.StartupPath, path));
            foreach (string Singlefolder in Folders)
            {
                if (Path.GetFileName(Singlefolder) == folder)
                {
                    folderpath = Singlefolder;
                }
            }
            return folderpath;
        }

    }
}
