﻿using System;
using System.Collections.Generic;
using System.IO;
using MailKit.Net.Imap;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit;

namespace Hajtek_mail_client_dev.libraries.dir_lib
{
    /*
     * IDEAL - File system 
     *  - USERS
     *      - au681767
     *          - Inbox
     *          - Spam
     *          - Drafts
     *          - Selvlavet mappe
     *     - Oliver
     *          - Inbox
     *          - Spam
     *          - Drafts
     *          - Selvlavet mappe
     *      - Asbjørn
     *          - Inbox
     *          - Spam
     *          - Drafts
     *          - Selvlavet mappe
    */


    /// <summary>
    /// <para>IDirManager</para>
    /// <para>Provides basic directory functionality</para>
    /// <para><c>void CreateSubfolder(string path, string name)</c></para>
    /// <para><c>void DeleteSubfolder()</c> </para>
    /// <para><c>string CreateUserFolder(string mail)</c></para>
    /// <para><c>string[] GetSubFolders(string folder)</c></para>
    /// </summary>
    interface IDirManager
    {
        void CreateSubfolder(string path, string name);
        void DeleteSubfolder();
        string CreateUserFolder(string mail);
        string[] GetSubFolders(string folder);
        string AccessSingleFolder(string path, string folder);
    }

    public abstract class Dir_mgmt : IDirManager
    {
        /*
         *  Creation of subfolder
         */
        public abstract void CreateSubfolder(string path, string name);

        /*
         * Creation of user folder
         */
        public abstract string CreateUserFolder(string mail);

        /*
         * Delete a folder
         */
        public abstract void DeleteSubfolder();

        /*
         * Find a specific subfolder
         */
        public abstract string[] GetSubFolders(string filename);

        /*
         * Returns path for single folder
         */
        public abstract string AccessSingleFolder(string path, string folder);

        /// <summary>
        /// Fetches the main Executable directory
        /// </summary>
        public string GetMainEXEPath()
        {
            //Gets install Install directory
            string MainDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            return MainDirectory;
        }        
    }
}
