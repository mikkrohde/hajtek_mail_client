﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hajtek_mail_client_dev.libraries.truncate
{
    public static class Truncate
    {
        public static string Truncate_func(this string value, int maxLength, string suffix = "...") //"..." sættes når string'en bliver cutted (kan ændres til alt, også bare ingenting, så sættes: "")
        {
            if (string.IsNullOrEmpty(value))
                return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength) + suffix;
        }
    }
}
