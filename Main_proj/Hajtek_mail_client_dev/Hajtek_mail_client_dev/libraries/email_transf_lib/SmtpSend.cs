﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using MailKit;
using MimeKit;
using MailKit.Net.Smtp;
using System.Web;
using System.IO;
using MailKit.Security;
using MailKit.Net.Imap;
using Hajtek_mail_client_dev.libraries.exceptions;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{
    class SmtpSend<T> : Mail_SMTP<T>
    {
        public int Mail_ID_parse
        {
            get { return MainWindow.Mail_ID; }
        }

        private UserData.UserInfo User;
        private UserData.Mailbox ActiveMailbox;

        public SmtpSend(UserData.UserInfo InitUser, ref UserData.Mailbox activemailbox)
        {
            User = InitUser;
            ActiveMailbox = activemailbox;
        }

        private string TextToHtml(string text)       //Get used when mail is in plain text and not HTML.
        {                                            //The cefbrowser can only show HTML, therefore a conversion from text to HTML is needed.
            text = HttpUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            return text;
        }

        //Til at sende bruger man Smtp, ikke Imap :D
        public override void Send_mail(string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI)
        {
            using (var smtp_client = new SmtpClient())
            {
                try
                {
                    smtp_client.Connect(User.Smtp_Host, User.Smtp_PORT, User.Use_ssl_SMTP);
                    smtp_client.Authenticate(User.AccountUsername, User.AccountPassword);
                }
                catch (Exception ex) when (
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentNullException ||
                ex is IOException ||
                ex is ArgumentException ||
                ex is ImapProtocolException ||
                ex is AuthenticationException ||
                ex is OperationCanceledException
                )
                {
                    throw new CoreExceptions.ConnectionException();
                }

                MimeMessage message = new MimeMessage();
                message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                message.Subject = subjectheaderUI;
                var TextBody = TextToHtml(mailtextbodyUI);
                message.Body = new TextPart("html") { Text = TextBody };

                foreach (var mailAddress in mailheaderUI)
                {
                    message.To.Add(MailboxAddress.Parse(mailAddress));
                    smtp_client.Send(message);
                }

                AppendToSmtpServer(message);
                WriteToEMLFile(message, User.CurrMailCount + 1);

                smtp_client.Disconnect(true);
            }
        }


        public override void Reply_To_mail(List<T> mailboxList, int mailid, string subjectheaderUI, string mailtextbodyUI)
        {

            using (var smtp_client = new SmtpClient())
            {
                try
                {
                    smtp_client.Connect(User.Smtp_Host, User.Smtp_PORT, User.Use_ssl_SMTP);
                    smtp_client.Authenticate(User.AccountUsername, User.AccountPassword);
                }
                catch (Exception ex) when (
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentNullException ||
                ex is IOException ||
                ex is ArgumentException ||
                ex is ImapProtocolException ||
                ex is AuthenticationException ||
                ex is OperationCanceledException
                )
                {
                    throw new CoreExceptions.ConnectionException();
                }

                Properties.Settings dynpaths = Properties.Settings.Default;

                Type typeParameter = typeof(T);

                if (typeParameter == typeof(UserData.InboxData))
                {
                    List<UserData.InboxData> usingMailbox = ActiveMailbox.Inboxdata;
                    MimeMessage message = new MimeMessage();

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;

                            message.To.Add(MailboxAddress.Parse(usingMailbox[mailidIndex].Mail_From_EmailAddress));
                            //Sendes tilbage til email adressen mailen kom fra
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;


                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                try { ForwardMessage = fetchmail.Load_Mail(dynpaths.InboxPath, mailid.ToString()); }
                                catch (Exception ex)
                                {
                                    throw;
                                }
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +

                            mailbody //Email to be replyed to
                            };

                            smtp_client.Send(message);
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }

                    }
                }

                else if (typeParameter == typeof(UserData.SentData))
                {
                    List<UserData.SentData> usingMailbox = ActiveMailbox.Sentdata;
                    MimeMessage message = new MimeMessage();

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.To.Add(MailboxAddress.Parse(usingMailbox[mailidIndex].Mail_From_EmailAddress)); //Sendes tilbage til email adressen mailen kom fra
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.SentPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                                "<br>" +
                                "-------- Original Message --------" +
                                "<br>" +
                                "<br>" +
                                "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                                "<br>" +
                                "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                                "<br>" +
                                "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                                "<br>" +
                                "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                                "<br>" +

                                mailbody //Email to be replyed to
                            };

                            smtp_client.Send(message);
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }

                else if (typeParameter == typeof(UserData.SpamData))
                {
                    

                    List<UserData.SpamData> usingMailbox = ActiveMailbox.Spamdata;
                    MimeMessage message = new MimeMessage();
                    
                    int mailidIndex;
                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;

                            message.To.Add(MailboxAddress.Parse(usingMailbox[mailidIndex].Mail_From_EmailAddress)); //Sendes tilbage til email adressen mailen kom fra
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;


                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.SpamPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +

                            mailbody //Email to be replyed to
                            };

                            smtp_client.Send(message);
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }


                else if (typeParameter == typeof(UserData.TrashData))
                {
                    List<UserData.TrashData> usingMailbox = ActiveMailbox.Trashdata;

                    MimeMessage message = new MimeMessage();

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.To.Add(MailboxAddress.Parse(usingMailbox[mailidIndex].Mail_From_EmailAddress)); //Sendes tilbage til email adressen mailen kom fra
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.TrashPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +

                            mailbody //Email to be replyed to
                            };

                            smtp_client.Send(message);
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }
                else
                {
                    //none of the types we expect exists in the list
                    return;
                }

                smtp_client.Disconnect(true);
            }
        }

        public override void Forward_mail(List<T> mailboxList, int mailid, string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI)
        {
            using (var smtp_client = new SmtpClient())
            {
                try
                {
                    smtp_client.Connect(User.Smtp_Host, User.Smtp_PORT, User.Use_ssl_SMTP);
                    smtp_client.Authenticate(User.AccountUsername, User.AccountPassword);
                }
                catch (Exception ex) when (
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentNullException ||
                ex is IOException ||
                ex is ArgumentException ||
                ex is ImapProtocolException ||
                ex is AuthenticationException ||
                ex is OperationCanceledException
                )
                {
                    throw new CoreExceptions.ConnectionException();
                }


                Type typeParameter = typeof(T);

                Properties.Settings dynpaths = Properties.Settings.Default;

                if (typeParameter == typeof(UserData.InboxData))
                {
                    List<UserData.InboxData> usingMailbox = ActiveMailbox.Inboxdata;

                    MimeMessage message = new MimeMessage();
                    MimeMessage ForwardMessage;
                    string mailbody;

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.InboxPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +
                            //Email to be replyed to
                            mailbody

                            };

                            foreach (var mailAddress in mailheaderUI)
                            {
                                message.To.Add(MailboxAddress.Parse(mailAddress));
                                smtp_client.Send(message);
                            }
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }
                else if (typeParameter == typeof(UserData.SentData))
                {
                    List<UserData.SentData> usingMailbox = ActiveMailbox.Sentdata;

                    MimeMessage message = new MimeMessage();

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.SentPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +
                            //Email to be replyed to
                            mailbody

                            };

                            foreach (var mailAddress in mailheaderUI)
                            {
                                message.To.Add(MailboxAddress.Parse(mailAddress));
                                smtp_client.Send(message);
                            }
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }
                else if (typeParameter == typeof(UserData.SpamData))
                {
                    List<UserData.SpamData> usingMailbox = ActiveMailbox.Spamdata;

                    MimeMessage message = new MimeMessage();

                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {
                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.SpamPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +
                            //Email to be replyed to
                            mailbody

                            };

                            foreach (var mailAddress in mailheaderUI)
                            {
                                message.To.Add(MailboxAddress.Parse(mailAddress));
                                smtp_client.Send(message);
                            }
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }
                else if (typeParameter == typeof(UserData.TrashData))
                {
                    List<UserData.TrashData> usingMailbox = ActiveMailbox.Trashdata;

                    MimeMessage message = new MimeMessage();
                    int mailidIndex;

                    for (int i = 0; i < usingMailbox.Count(); i++)
                    {

                        if (usingMailbox[i].Mail_ID == Mail_ID_parse)
                        {
                            mailidIndex = i;
                            message.From.Add(new MailboxAddress(User.FullName, User.MailAddress));
                            message.Subject = subjectheaderUI;

                            MimeMessage ForwardMessage;
                            string mailbody;
                            using (ImapMailOP fetchmail = new ImapMailOP(User))
                            {
                                ForwardMessage = fetchmail.Load_Mail(dynpaths.TrashPath, mailid.ToString());
                            }

                            //Email to be replyed to
                            if (ForwardMessage.HtmlBody != null)
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }
                            else
                            {
                                mailbody = ForwardMessage.HtmlBody;
                            }

                            message.Body = new TextPart("html")
                            {
                                Text = TextToHtml(mailtextbodyUI) + //New email
                            "<br>" +
                            "-------- Original Message --------" +
                            "<br>" +
                            "<br>" +
                            "<b>Subject: </b>" + usingMailbox[mailidIndex].Mail_Subject + //Skal rettes! Find rigtige sti/biding
                            "<br>" +
                            "<b>Date: </b>" + usingMailbox[mailidIndex].Mail_Date_Time +
                            "<br>" +
                            "<b>From: </b>" + usingMailbox[mailidIndex].Mail_From +
                            "<br>" +
                            "<b>To: </b>" + User.FullName + " <" + User.MailAddress + "> " +
                            "<br>" +
                            //Email to be replyed to
                            mailbody

                            };

                            foreach (var mailAddress in mailheaderUI)
                            {
                                message.To.Add(MailboxAddress.Parse(mailAddress));
                                smtp_client.Send(message);
                            }
                            AppendToSmtpServer(message);
                            WriteToEMLFile(message, mailidIndex);
                        }
                    }
                }
                else
                {
                    //do nothing
                }

                smtp_client.Disconnect(true);
            }

        }

        private void WriteToEMLFile(MimeMessage message, int mailid)
        {

            //Get all files in the inbox folder
            string sentpath = Properties.Settings.Default.SentPath;
            string[] allMailsInSent = Directory.GetFiles(sentpath);
            List<int> sentmailtemp = new List<int>();

            foreach (string mails in allMailsInSent)
            {
                var tmp = Path.GetFileName(mails).Split('_')[0];
                if (int.TryParse(tmp, out int res))
                {
                    sentmailtemp.Add(res);
                }
            }

            int NewName = sentmailtemp.Max() + 1;
            string fullfilecreate = Path.Combine(sentpath, string.Format("{0}_{1}.eml", NewName, 32));

            using (FileStream fs = File.Create(fullfilecreate))
            {
                message.WriteTo(fs);
            }
        }

        private void AppendToSmtpServer(MimeMessage MailToAppend)
        {
            using (ImapClient Client = new ImapClient())
            {
                try
                {
                    if (Client.IsConnected != true)
                    {
                        Client.Connect(User.Imap_Host, User.Imap_PORT, SecureSocketOptions.SslOnConnect);
                        Client.Authenticate(User.AccountUsername, User.AccountPassword);
                    }

                    IMailFolder sent = Client.GetFolder(SpecialFolder.Sent);


                    sent.Append(MailToAppend);

                }
                catch (Exception ex) when (ex is AuthenticationException ||
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentException)
                {
                    throw;
                }
            }
        }



        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~SmtpSend()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public override void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
