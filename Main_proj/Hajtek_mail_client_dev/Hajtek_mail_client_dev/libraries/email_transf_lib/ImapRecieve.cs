﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using Hajtek_mail_client_dev.libraries.dir_lib;
using Hajtek_mail_client_dev.libraries.exceptions;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using MailKit.Security;
using MailKit;
using MimeKit;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{

    /// <summary>
    /// <para>Class for receiving mails</para>
    /// <para>Methods</para>
    /// <list type="bullet">
    /// <item><param name="M">void Receive_mail()</param></item>
    /// <item><param name="M">void Receive_mail(string specialfolder)</param></item>
    /// <item><param name="M">void Dispose()</param></item>
    /// </list>
    /// </summary>
    public class DefImapRecieve : Mail_IMAP{

        protected ImapClient ReceiveClient = new ImapClient();
        protected int newEmailCount = 0;
        private UserData.UserInfo User;
        private string inboxFolder;
        private int NewMailsReceived = 0;
        private int NewMailCount = 0;
        private int MailDiff = 0;
        private int CollectiveNewMailsReceived = 0;

        /*
         *  Creates a filestream for parsing,
         *  new .eml files into the inbox folder
         */
        private FileStream fs;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InitUser"></param>
        public DefImapRecieve(UserData.UserInfo InitUser) {
            if(InitUser.AccountUsername == null ||
               InitUser.AccountPassword == null ||
               InitUser.Imap_Host == null ||
               InitUser.Imap_PORT == 0)
            {
                //Throws an exception since the arguments arent valid
                try
                {
                    throw new MailLibException();
                }
                catch (MailLibException.UserArgumentNULLException ex)
                {
                    //ensures that the exception thrown is actually the exception we want to recover from.
                    if (ex is MailLibException.UserArgumentNULLException)
                    {
                        //return from the constructor, such that it won't get initialized
                        return;
                    }
                }
            }
            else
            {
                //initialize the user
                User = InitUser;
            }
        }

        /// <summary>
        /// Recieve mail from Imap host: <c>Inbox</c>
        /// </summary>
        /// <param name="User"></param>
        /// <returns>Number of new mails received</returns>
        public override int Receive_mail(ref UserData.UserInfo CurrActiveUser)
        {
            TestConnectAuth();
            /*
             * 1. Create new filestream
             * 2. Pull mail from inbox 1 by 1
             *    - check if its already in the inbox or any other folder
             * 3. If it isn't stored anywhere, use mimekit.mimemessage.WriteTo, to write it down as an eml file
             */

            //Ensures access to userfolder
            IDirManager dir = new DirMgmtLocal();

            //string pathtest = string.("USERS", User.AccountUsername);

            string[] subfolders = dir.GetSubFolders(Path.Combine("USERS", User.AccountUsername));

            try
            {
                foreach (string folders in subfolders)
                {
                    /*
                     * if the name of one of the folder in the list of subfolders are Inbox
                     * set inboxFolder =
                    */
                    if (Path.GetFileName(folders) == "Inbox")
                    {
                        inboxFolder = folders;
                    }

                }

                //checks if we have found the actual
                if (Path.GetFileName(inboxFolder) != "Inbox")
                {
                    //creates new subfolder in the users main folder
                    string[] UserFolders = dir.GetSubFolders("USERS");

                    foreach (string users in UserFolders)
                    {
                        //checks which of the users are active
                        if (Path.GetFileName(users) == User.AccountUsername)
                        {
                            //creates a new Inbox folder in the users directory
                            dir.CreateSubfolder(Path.GetFullPath(users), "Inbox");
                            inboxFolder = Path.Combine(users, "Inbox");
                        }
                    }

                }
                //throw new MailLibException();
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                //Rethrow
                throw;
            }

            //Open inbox folder for retrieval
            ReceiveClient.Inbox.Status(StatusItems.Unread);
            ReceiveClient.Inbox.Open(FolderAccess.ReadOnly);

            NewMailCount = ReceiveClient.Inbox.Count; //doesn't get the value from the server

            CurrActiveUser.CurrMailUnReadCount = ReceiveClient.Inbox.Unread;

            //calculates difference in new mails and old mail count.
            MailDiff = NewMailCount - User.CurrMailCount;

            IList<IMessageSummary> mails;

            //if the maildiff.
            if (MailDiff > 0)
            {
                //get the 
                try
                {
                    ReceiveClient.Inbox.Fetch(User.CurrMailCount, NewMailCount, MessageSummaryItems.Flags | MessageSummaryItems.UniqueId);
                }
                catch(ImapCommandException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.ResponseText);
                }
                finally
                {
                    mails = ReceiveClient.Inbox.Fetch(User.CurrMailCount, NewMailCount, MessageSummaryItems.Flags | MessageSummaryItems.UniqueId);
                }

                foreach (IMessageSummary mail in mails)
                {
                    //counts the new mails received
                    NewMailsReceived++;
                    //Fetch each messages unique id


                    //somehow a possibility of deadlock... dont know why.
                    MimeMessage message = ReceiveClient.Inbox.GetMessage(mail.UniqueId);

                    int FlagVal = (int)mail.Flags.Value;

                    //filename
                    string FileName = Path.Combine(inboxFolder, mail.UniqueId.Id.ToString());

                    //spamfilter.filter();

                    // write the message to a file and store it in the filestream
                    using (fs = File.Create(string.Format("{0}_{1}.eml", FileName, FlagVal))){
                        message.WriteTo(fs);
                    }
                }

                //Disconnect from mailserver
                ReceiveClient.Disconnect(true);
                User.CurrMailCount = NewMailCount;
            }
            else
            {
                //Disconnect from mailserver
                ReceiveClient.Disconnect(true);
            }

            //returns the amount of new mails received
            return NewMailsReceived;
        }


        #region

        /// <summary>
        /// Class Structure to contain all specialfolder attributes.
        /// Both fetched from imap server and local data.
        /// </summary>
        public struct SFLocalAttributes
        {
            public SFLocalAttributes(IMailFolder newFolder, string path, int mailcount)
            {
                Folder = newFolder;
                Path = path;
                Mailcount = mailcount;
            }

            public IMailFolder Folder { get; set; }
            public string Path { get; set; }
            public int Mailcount { get; set; }
        }
        /// <summary>
        /// recieve mail from specific any imap host 
        /// <para>Stores the newly received mails in the local filesystem</para>
        /// </summary>
        /// <param name="foldernames"></param>
        /// <returns>Number of new mails received</returns>
        public override int Receive_mail(ref UserData.UserInfo user, bool specialfolders, params string[] foldernames)
        {

            // Get the first personal namespace and list the toplevel folders under it.
            try
            {
                TestImapConnection();
            }
            catch(Exception ex) when (
            ex is System.Net.Sockets.SocketException||
            ex is AuthenticationException) 
            {
                throw;
            }

            TestConnectAuth();

            Properties.Settings DynPaths = Properties.Settings.Default;

            if ((ReceiveClient.Capabilities & (ImapCapabilities.SpecialUse | ImapCapabilities.XList)) != 0)
            {
                List<SFLocalAttributes> cSpecialFolders = new List<SFLocalAttributes>();
                var personal = ReceiveClient.GetFolder(ReceiveClient.PersonalNamespaces[0]);

                string tmppath;
                int tempcount;
                SFLocalAttributes SFLAtmp;
                //Fetch each specialfolder from the IMAP server and store them in a list.
                foreach (IMailFolder folder in personal.GetSubfolders(false))
                {
                    Console.WriteLine("[folder] {0}", folder.Name);
                    if (foldernames.Contains(folder.Name))
                    {
                        if(folder.Name == "Sent")
                        {
                            tmppath = DynPaths.SentPath;
                            tempcount = User.CurrSentCount;
                        }else if(folder.Name == "Drafts")
                        {
                            tmppath = DynPaths.DraftPath;
                            tempcount = User.CurrDraftCount;
                        }
                        else if(folder.Name == "Trash")
                        {
                            tmppath = DynPaths.TrashPath;
                            tempcount = User.CurrTrashCount;
                        }
                        else if (folder.Name == "Archive")
                        {
                            tmppath = DynPaths.ArchivePath;
                            tempcount = User.CurrArchiveCount;
                        }
                        else if (folder.Name == "Junk")
                        {
                            tmppath = DynPaths.SpamPath;
                            tempcount = User.CurrSpamCount;
                        }
                        else
                        {
                            //we only want these specific specialfolders
                            continue;
                        }

                        SFLAtmp = new SFLocalAttributes(folder, tmppath, tempcount);
                        cSpecialFolders.Add(SFLAtmp);
                    }
                }

                IList<IMessageSummary> mails;
                //Go through each of the specialmails and fetch the new mails:
                foreach (SFLocalAttributes folder in cSpecialFolders)
                {

                    //Opens the specific folder.
                    IMailFolder tmpfolder = folder.Folder;

                    tmpfolder.Open(FolderAccess.ReadOnly);
                    int tmpMailsReceived = 0;
                    int newcount = tmpfolder.Count;

                    MailDiff = newcount - folder.Mailcount;

                    if(MailDiff > 0)
                    {
                        //Fetch the mails
                        try
                        {
                            tmpfolder.Fetch(folder.Mailcount, newcount, MessageSummaryItems.Flags | MessageSummaryItems.UniqueId);
                        }
                        catch (ImapCommandException ex)
                        {
                            Console.WriteLine(ex.Message);
                            Console.WriteLine(ex.ResponseText);
                        }
                        finally
                        {
                            mails = tmpfolder.Fetch(folder.Mailcount, newcount, MessageSummaryItems.Flags | MessageSummaryItems.UniqueId);
                        }

                        foreach (IMessageSummary mail in mails)
                        {
                            //counts the new mails received
                            tmpMailsReceived++;
                            //Fetch each messages unique id

                            //somehow a possibility of deadlock... dont know why.
                            MimeMessage message = tmpfolder.GetMessage(mail.UniqueId);

                            int FlagVal = (int)mail.Flags.Value;

                            //filename
                            string FileName = Path.Combine(folder.Path, mail.UniqueId.Id.ToString());

                            //spamfilter.filter();

                            // write the message to a file and store it in the filestream
                            using (fs = File.Create(string.Format("{0}_{1}.eml", FileName, FlagVal)))
                            {
                                message.WriteTo(fs);
                            }
                        }

                        if(folder.Folder.Name == "Sent")
                        {
                            user.CurrSentCount += tmpMailsReceived;
                        }else if (folder.Folder.Name == "Drafts")
                        {
                            user.CurrDraftCount += tmpMailsReceived;
                        }
                        else if (folder.Folder.Name == "Trash")
                        {
                            user.CurrTrashCount += tmpMailsReceived;
                        }
                        else if (folder.Folder.Name == "Archive")
                        {
                            user.CurrArchiveCount += tmpMailsReceived;
                        }
                        else if (folder.Folder.Name == "Junk")
                        {
                            user.CurrSpamCount += tmpMailsReceived;
                        }
                        else
                        {
                            //we only want these specific specialfolders
                            continue;
                        }

                    }
                }
            }
            
            ReceiveClient.Disconnect(true);

            return CollectiveNewMailsReceived;
        }


        #endregion

        /// <summary>
        /// <para>Fetches all the specialfolders on the imap server</para>
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        protected List<IMailFolder> FetchSpecialFolders(ImapClient client)
        {
            List<IMailFolder> allFolders = new List<IMailFolder>();
            
            if ((ReceiveClient.Capabilities & (ImapCapabilities.SpecialUse | ImapCapabilities.XList)) != 0)
            {
                foreach (IMailFolder folders in client.GetFolder(SpecialFolder.All))
                {
                    allFolders.Add(folders);
                }
            }

            return allFolders;
        }


        //Imap Connection and Authentication
        #region Connection & Authentication methods
        protected void ConnectAuth()
        {
            ReceiveClient.Connect(User.Imap_Host, User.Imap_PORT, SecureSocketOptions.SslOnConnect);
            ReceiveClient.Authenticate(User.AccountUsername, User.AccountPassword);
        }

        private void TestConnectAuth()
        {
            try{
                ConnectAuth();
            }catch(Exception exc) when (exc is ArgumentException || exc is System.Net.Sockets.SocketException)
            {
                if(exc is ObjectDisposedException || exc is ArgumentException){return;
                }
                else
                {
                    throw;
                } 
            }
        }

        /// <summary>
        /// Completely identical to the one above....
        /// This method is used for debug purposes: delete when it becomes unnecessary
        /// </summary>
        /// <exception cref="System.Net.Sockets.SocketException"></exception>
        /// <exception cref="AuthenticationException"></exception>
        /// <returns>Mailbox_data</returns>
        public bool TestImapConnection()
        {
            try
            {
                using (var Client_Test = new ImapClient())
                {
                    Client_Test.Connect(User.Imap_Host, User.Imap_PORT, SecureSocketOptions.SslOnConnect);
                    Client_Test.Authenticate(User.AccountUsername, User.AccountPassword);
                }
            }
            catch (Exception ex)
            when(ex is System.Net.Sockets.SocketException ||
                 ex is AuthenticationException)
            {
                throw;
            }

            return true;
        }


        #endregion

        //Contains the methods which are called when the class Def_
        #region Dispose methods (Garbage collecter methods)

        //Dispose methods for managing the resources !NEEDS IMPLEMENTATION!
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //TODO: dispose managed state (managed objects)
                    //client
                    ReceiveClient.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ImapRecieve()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }
        public override void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion


    }






}
