﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.exceptions;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using MailKit.Net.Imap;
using MailKit.Security;
using MimeKit;
using MailKit;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{
    public class ImapMailOP : Mail_OP
    {
        protected ImapClient MailOPClient = new ImapClient();
        private UserData.UserInfo User;

        public ImapMailOP(UserData.UserInfo InitUser)
        {
            if (InitUser.AccountUsername == null ||
               InitUser.AccountPassword == null ||
               InitUser.Imap_Host == null ||
               InitUser.Imap_PORT == 0 ||
               InitUser.Use_ssl_IMAP == false)
            {
                //Throws an exception since the arguments arent valid
                try
                {
                    throw new MailLibException();
                }
                catch (MailLibException.UserArgumentNULLException ex)
                {
                    //ensures that the exception thrown is actually the exception we want to recover from.
                    if (ex is MailLibException.UserArgumentNULLException)
                    {
                        //return from the constructor, such that it won't get initialized
                        return;
                    }
                }
            }
            else
            {
                //initialize the user
                User = InitUser;
            }
        }

        #region LOAD LOCAL MAILS:

        /// <summary>
        /// Load Local Inbox Method 
        /// - fetches all mails from USERS/CoreActiveUser
        /// CONVERT THIS TO AN ASYNCRONOUS METHODS TO ENSURE RESPONSIVE UI WHILE RUNNING THIS METHOD
        /// //Mailuid is the list index of the entire 
        /// Tracks if what mails have appeared an which have disappeared
        /// - If it have disappeared in the array its the index is == null
        /// - else if a new mail have appeared its defined at the index UID
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Inbox(ref UserData.Mailbox ListMailboxes) //figure out how pointers work.
        {
            /* List of stuff to pull from mimemessage
             * 1. mail sender
             * 2. subject
             * 3. date and time
             * 4. mail id
             */

            /* Algorithm: fetch mail summary
            * 1. Get path based on Active user
            * 2. Get a pointer to the active core usermailbox
            * 3. Check the new mailcount in the USERS, Inbox folder
            * 4. Get the highest uniqueID value from the mails. This will be our list allocation size for the first initiation of the list.
            * 5. If i the list have already been initialized if(list == null) else if(!list.empty()) else{the list exists and have values}
            * 6. check if the highest uniqueID is greater than the allocated amount, if it is reallocate the the double array size amount.
            * 7. in case that its actually smaller dont to anything
            */

            //Fetches InboxPath
            var inboxpath = Properties.Settings.Default.InboxPath;
            
            //Get all files in the inbox folder
            string[] allMailsInInbox = Directory.GetFiles(inboxpath);


            if (Properties.Settings.Default.FirstLaunch == true)
            {
                return;
            }

            if(allMailsInInbox.Length == 0)
            {
                return;
            }


            List<int> allmailstemp = new List<int>();

            foreach (string mails in allMailsInInbox)
            {
                var tmp = Path.GetFileName(mails).Split('_')[0];
                if (int.TryParse(tmp, out int res))
                {
                    allmailstemp.Add(res);
                }
            }

            allmailstemp = allmailstemp.OrderBy(p => p).ToList();


            //get the highest value in MailsName
            int newHighestUID = allmailstemp.Max();
            //create new tmp userdata.inboxdata struct.

            UserData.InboxData tmpInbox = new UserData.InboxData();

            //string array conversion for comparison

            string[] MailsInInboxName = new string[newHighestUID + 1]; //define new array with the size of allmaiulsininbox variable
            string[] MailsInInbox_FullFilename = new string[newHighestUID + 1];
            //get all filenames such that we only have the UNIQUE ID of the mails:
            for (int i = 0; i <= allMailsInInbox.Count() - 1; ++i)
            {

                //store them in a new array.
                var INDEX = Path.GetFileName(allMailsInInbox[i]).Split('_')[0];

                var index = int.Parse(INDEX);

                MailsInInboxName[index] = allMailsInInbox[i];

                MailsInInbox_FullFilename[index] = Path.GetFileName(allMailsInInbox[i]);

            }


            //Checks if the inbox is empty
            if (ListMailboxes.Inboxdata == null || ListMailboxes.Spamdata.Count() == 0)
            {
                List<UserData.InboxData> tmp = new List<UserData.InboxData>();
                ListMailboxes.Inboxdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //

                //

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    //Load that dang message
                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;


                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];


                    tmpInbox.Mail_ID = UIDs;
                    tmpInbox.IsRead = IsRead(filename);

                    ListMailboxes.Inboxdata.Add(tmpInbox);

                    ++uidPosCounter;
                }

            }
            else if (ListMailboxes.Inboxdata.Count() < allmailstemp.Count())
            {
                //initialize path tracking counter.
                //int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                for (int i = ListMailboxes.Inboxdata.Count(); i < allmailstemp.Count(); i++)
                {

                    UserData.InboxData NewTmp = new UserData.InboxData();

                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[i];
                    //Load that dang message
                    MimeMessage messages = MimeMessage.Load(path);

                    NewTmp.Mail_From = messages.From[0].Name;
                    NewTmp.Mail_Subject = messages.Subject;
                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    NewTmp.Mail_Date_Time = temp[0];
                    NewTmp.Mail_ID = allmailstemp[i];
                    NewTmp.IsRead = IsRead(MailsInInbox_FullFilename[i]);

                    if (NewTmp.Mail_Date_Time != null)
                    {
                        ListMailboxes.Inboxdata.Add(NewTmp);
                    }
                }
            }
            else
            {
                List<UserData.InboxData> tmp = new List<UserData.InboxData>();
                ListMailboxes.Inboxdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //

                //

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    //Load that dang message
                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;

                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];

                    tmpInbox.Mail_ID = UIDs;
                    tmpInbox.IsRead = IsRead(filename);

                    ListMailboxes.Inboxdata.Add(tmpInbox);

                    ++uidPosCounter;
                }
            }

            return;
        }

        /// <summary>
        /// Load Local Sent mails
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Sent(ref UserData.Mailbox ListMailboxes)
        {
            //Fetches InboxPath
            var sentpath = Properties.Settings.Default.SentPath;

            if (Properties.Settings.Default.FirstLaunch == true)
            {
                return;
            }


            //Get all files in the inbox folder
            string[] allMailsInInbox = Directory.GetFiles(sentpath);

            if (allMailsInInbox.Length == 0)
            {
                return;
            }

            
            List<int> allmailstemp = new List<int>();

            foreach (string mails in allMailsInInbox)
            {
                var tmp = Path.GetFileName(mails).Split('_')[0];
                if (int.TryParse(tmp, out int res))
                {
                    allmailstemp.Add(res);
                }
            }

            allmailstemp = allmailstemp.OrderBy(p => p).ToList();


            //get the highest value in MailsName
            int newHighestUID = allmailstemp.Max();
            //create new tmp userdata.inboxdata struct.

            UserData.SentData tmpInbox = new UserData.SentData();

            //string array conversion for comparison

            string[] MailsInInboxName = new string[newHighestUID + 1]; //define new array with the size of allmaiulsininbox variable
            string[] MailsInInbox_FullFilename = new string[newHighestUID + 1];
            //get all filenames such that we only have the UNIQUE ID of the mails:
            for (int i = 0; i <= allMailsInInbox.Count() - 1; ++i)
            {

                //store them in a new array.
                var INDEX = Path.GetFileName(allMailsInInbox[i]).Split('_')[0];

                var index = int.Parse(INDEX);

                MailsInInboxName[index] = allMailsInInbox[i];

                MailsInInbox_FullFilename[index] = Path.GetFileName(allMailsInInbox[i]);

            }

            //Checks if the inbox is empty
            if (ListMailboxes.Sentdata == null || ListMailboxes.Sentdata.Count() == 0)
            {

                List<UserData.SentData> tmp = new List<UserData.SentData>();
                ListMailboxes.Sentdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();
                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //

                //

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    //Load that dang message
                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;

                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];

                    tmpInbox.Mail_ID = UIDs;
                    tmpInbox.IsRead = IsRead(filename);


                    ListMailboxes.Sentdata.Add(tmpInbox);

                    ++uidPosCounter;
                }

            }
            else if (ListMailboxes.Sentdata.Count() < allmailstemp.Count())
            {
                //initialize path tracking counter.
                //int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                for (int i = ListMailboxes.Sentdata.Count(); i < allmailstemp.Count(); i++)
                {

                    UserData.SentData NewTmp = new UserData.SentData();

                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[i];
                    var filename = MailsInInbox_FullFilename[i];
                    //Load that dang message
                    MimeMessage messages = MimeMessage.Load(path);

                    NewTmp.Mail_From = messages.From[0].Name;
                    NewTmp.Mail_Subject = messages.Subject;
                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    NewTmp.Mail_Date_Time = temp[0];
                    NewTmp.Mail_ID = allmailstemp[i];
                    NewTmp.IsRead = IsRead(filename);

                    if (NewTmp.Mail_Date_Time != null)
                    {
                        ListMailboxes.Sentdata.Add(NewTmp);
                    }
                }
            }
            return;
        }

        /// <summary>
        /// Load Local Draft mails 
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Draft(ref UserData.Mailbox ListMailboxes)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Load Local Spam mails 
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Spam(ref UserData.Mailbox ListMailboxes)
        {
            //Fetches InboxPath
            var spampath = Properties.Settings.Default.SpamPath;

            if (Properties.Settings.Default.FirstLaunch == true)
            {
                return;
            }

            //Get all files in the inbox folder
            string[] allMailsInInbox = Directory.GetFiles(spampath);

            if (allMailsInInbox.Length == 0)
            {
                return;
            }

            List<int> allmailstemp = new List<int>();

            foreach (string mails in allMailsInInbox)
            {
                var tmp = Path.GetFileName(mails).Split('_')[0];
                if (int.TryParse(tmp, out int res))
                {
                    allmailstemp.Add(res);
                }
            }

            allmailstemp = allmailstemp.OrderBy(p => p).ToList();


            //get the highest value in MailsName
            int newHighestUID = allmailstemp.Max();
            //create new tmp userdata.inboxdata struct.

            UserData.SpamData tmpInbox = new UserData.SpamData();

            //string array conversion for comparison

            string[] MailsInInboxName = new string[newHighestUID + 1]; //define new array with the size of allmaiulsininbox variable
            string[] MailsInInbox_FullFilename = new string[newHighestUID + 1];
            //get all filenames such that we only have the UNIQUE ID of the mails:
            for (int i = 0; i <= allMailsInInbox.Count() - 1; ++i)
            {

                //store them in a new array.
                var INDEX = Path.GetFileName(allMailsInInbox[i]).Split('_')[0];

                var index = int.Parse(INDEX);

                MailsInInboxName[index] = allMailsInInbox[i];

                MailsInInbox_FullFilename[index] = Path.GetFileName(allMailsInInbox[i]);

            }

            //Checks if the inbox is empty
            if (ListMailboxes.Spamdata == null || ListMailboxes.Spamdata.Count() == 0)
            {

                List<UserData.SpamData> tmp = new List<UserData.SpamData>();
                ListMailboxes.Spamdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();
                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //

                //

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    //Load that dang message
                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;

                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];

                    tmpInbox.IsRead = IsRead(filename);


                    tmpInbox.Mail_ID = UIDs;


                    ListMailboxes.Spamdata.Add(tmpInbox);

                    ++uidPosCounter;
                }

            }
            else if (ListMailboxes.Spamdata.Count() < allmailstemp.Count())
            {
                //initialize path tracking counter.
                //int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                for (int i = ListMailboxes.Spamdata.Count(); i < allmailstemp.Count(); i++)
                {

                    UserData.SpamData NewTmp = new UserData.SpamData();

                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[i];
                    //Load that dang message
                    MimeMessage messages = MimeMessage.Load(path);

                    NewTmp.Mail_From = messages.From[0].Name;
                    NewTmp.Mail_Subject = messages.Subject;
                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    NewTmp.Mail_Date_Time = temp[0];
                    NewTmp.Mail_ID = allmailstemp[i];
                    tmpInbox.IsRead = IsRead(MailsInInbox_FullFilename[i]);

                    if (NewTmp.Mail_Date_Time != null)
                    {
                        ListMailboxes.Spamdata.Add(NewTmp);
                    }
                }
            }
            else
            {
                List<UserData.SpamData> tmp = new List<UserData.SpamData>();
                ListMailboxes.Spamdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();
                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //

                //

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    //Load that dang message
                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;


                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];
                    tmpInbox.IsRead = IsRead(filename);


                    tmpInbox.Mail_ID = UIDs;


                    ListMailboxes.Spamdata.Add(tmpInbox);

                    ++uidPosCounter;
                }
            }

            return;
        }

        /// <summary>
        /// Load Local Archive mails
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Archive(ref UserData.Mailbox ListMailboxes)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Load Local Trash mails
        /// </summary>
        /// <param name="ListMailboxes"></param>
        public override void LoadLocal_Trash(ref UserData.Mailbox ListMailboxes)
        {
            //Fetches InboxPath
            var trashpath = Properties.Settings.Default.TrashPath;

            if (Properties.Settings.Default.FirstLaunch == true)
            {
                return;
            }

            //Get all files in the inbox folder
            string[] allMailsInInbox = Directory.GetFiles(trashpath);

            if (allMailsInInbox.Length == 0)
            {
                return;
            }

            List<int> allmailstemp = new List<int>();

            foreach (string mails in allMailsInInbox)
            {
                var tmp = Path.GetFileName(mails).Split('_')[0];
                if (int.TryParse(tmp, out int res))
                {
                    allmailstemp.Add(res);
                }
            }

            allmailstemp = allmailstemp.OrderBy(p => p).ToList();


            //get the highest value in MailsName
            int newHighestUID = allmailstemp.Max();
            //create new tmp userdata.inboxdata struct.

            UserData.TrashData tmpInbox = new UserData.TrashData();

            //string array conversion for comparison

            string[] MailsInInboxName = new string[newHighestUID + 1]; //define new array with the size of allmaiulsininbox variable
            string[] MailsInInbox_FullFilename = new string[newHighestUID + 1];
            //get all filenames such that we only have the UNIQUE ID of the mails:
            for (int i = 0; i <= allMailsInInbox.Count() - 1; ++i)
            {

                //store them in a new array.
                var INDEX = Path.GetFileName(allMailsInInbox[i]).Split('_')[0];

                var index = int.Parse(INDEX);

                MailsInInboxName[index] = allMailsInInbox[i];

                MailsInInbox_FullFilename[index] = Path.GetFileName(allMailsInInbox[i]);

            }

            //Checks if the inbox is empty
            if (ListMailboxes.Trashdata == null || ListMailboxes.Trashdata.Count() == 0)
            {

                List<UserData.TrashData> tmp = new List<UserData.TrashData>();
                ListMailboxes.Trashdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();
                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;

                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];


                    tmpInbox.Mail_ID = UIDs;

                    tmpInbox.IsRead = IsRead(filename);

                    ListMailboxes.Trashdata.Add(tmpInbox);

                    ++uidPosCounter;
                }

            }
            else if (ListMailboxes.Trashdata.Count() < allmailstemp.Count())
            {
                //initialize path tracking counter.
                //int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();

                for (int i = ListMailboxes.Trashdata.Count(); i < allmailstemp.Count(); i++)
                {
                    UserData.TrashData NewTmp = new UserData.TrashData();

                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[i];
                    //Load that dang message
                    MimeMessage messages = MimeMessage.Load(path);

                    NewTmp.Mail_From = messages.From[0].Name;
                    NewTmp.Mail_Subject = messages.Subject;
                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    NewTmp.Mail_Date_Time = temp[0];
                    NewTmp.Mail_ID = allmailstemp[i];
                    NewTmp.IsRead = IsRead(MailsInInbox_FullFilename[i]);

                    if (NewTmp.Mail_Date_Time != null)
                    {
                        ListMailboxes.Trashdata.Add(NewTmp);
                    }
                }
            }
            else
            {
                List<UserData.TrashData> tmp = new List<UserData.TrashData>();
                ListMailboxes.Trashdata = tmp;
                //Make the new list size double the size highest userid.
                //UserData.InboxData NewTmp = new UserData.InboxData();
                //UserData.InboxData[] TmpArr = new UserData.InboxData[MailsName.Count() + 1];

                int[] LoadFails = new int[allmailstemp.Count()];
                MimeMessage messages;

                //initialize path tracking counter.
                int uidPosCounter = 0;

                MailsInInboxName = MailsInInboxName.Where(c => c != null).ToArray();
                MailsInInbox_FullFilename = MailsInInbox_FullFilename.Where(c => c != null).ToArray();
                /*
                 * Inserts all elements from one list to the other using the UNIQUEID.
                 * To ensure all indices matches and inserts the values.
                 */

                //for (int i = 1; i != allmailstemp.Count(); ++i)
                foreach (int UIDs in allmailstemp)
                {
                    //get the path based on the UIDposCounter
                    var path = MailsInInboxName[uidPosCounter];
                    var filename = MailsInInbox_FullFilename[uidPosCounter];


                    if (path == null || filename == null)
                    {
                        ++uidPosCounter;
                        continue;
                    }

                    try
                    {
                        messages = MimeMessage.Load(path);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        LoadFails[UIDs] = UIDs;
                        continue;
                    }

                    //store it in tmp InboxData variable
                    tmpInbox.Mail_From = messages.From[0].Name;
                    tmpInbox.Mail_From_EmailAddress = messages.From.OfType<MailboxAddress>().Single().Address.ToString();
                    tmpInbox.Mail_Subject = messages.Subject;


                    string[] temp = messages.Date.DateTime.ToLocalTime().ToString().Split(' ');
                    tmpInbox.Mail_Date_Time = temp[0];


                    tmpInbox.Mail_ID = UIDs;
                    tmpInbox.IsRead = IsRead(filename);

                    ListMailboxes.Trashdata.Add(tmpInbox);

                    ++uidPosCounter;
                }
            }

            return;
        }


        #endregion

        public override MimeMessage Load_Mail(string folderpath, string filename)
        {
            MimeMessage LoadedMessagetest;
            FileInfo[] FilesInDir;
            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(folderpath);
            string PartialPath = Path.Combine(folderpath, filename);

            try
            {
                FilesInDir = DirectoryInWhichToSearch.GetFiles(filename + "_*");
                var getfullname = FilesInDir[0].FullName;
            }
            catch (Exception ex)
            when (
            ex is System.Xaml.XamlParseException ||
            ex is IOException || ex is ArgumentNullException)
            {
                //rethrows for exception handling in Core
                throw;
            }

            string FullPath = FilesInDir[0].FullName;

            //test to see if we can even load the folder.
            try
            {
                MimeMessage.Load(FullPath);
            }
            catch (Exception ex)
            when (ex is IOException ||
                  ex is DirectoryNotFoundException ||
                  ex is FileNotFoundException ||
                  ex is System.Xaml.XamlParseException)
            {
                //rethrows for exception handling in Core
                throw;
            }

            LoadedMessagetest = MimeMessage.Load(FullPath);

            return LoadedMessagetest;
        }



        public override bool IsRead(string filename)
        {
            string[] flag_number_string = filename.Split('_', '.');

            int flag_number = int.Parse(flag_number_string[1]);

            if (flag_number % 2 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Flags mail with the specific UniqueID to be deleted and moves it to the delete folder.
        /// - the unique id is the 
        /// </summary>
        /// <param name="OldFilePath"></param>
        /// <param name="Mail_ID"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public override bool MoveToTrashFolder(string OldFilePath, string Mail_ID)
        {
            TestConnectAuth();

            /*
             * Process
             * 1. Move to deleted folder
             * 2. Add imap flag for deleted
             */

            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(OldFilePath);
            FileInfo[] filesInDir = DirectoryInWhichToSearch.GetFiles(Mail_ID + "*");
            string TrashPath = Properties.Settings.Default.TrashPath;

            string OldPathComplete = Path.GetFullPath(filesInDir[0].FullName);

            try
            {
                /*
                 * 1. move to local trash folder
                 *    - get the trashpaths highest uid
                 * 2. flag as deleted messages on imap folder.
                 */

                //Get all files in the inbox folder
                string[] allMailsInInbox = Directory.GetFiles(TrashPath);
                List<int> allmailstemp = new List<int>();

                foreach (string mails in allMailsInInbox)
                {
                    var tmp = Path.GetFileName(mails).Split('_')[0];
                    if (int.TryParse(tmp, out int res))
                    {
                        allmailstemp.Add(res);
                    }
                }

                allmailstemp = allmailstemp.OrderBy(p => p).ToList();

                //get the highest value in MailsName
                int partnewfilename = allmailstemp.Max() + 1;

                string newfilename = partnewfilename + "_1.eml";

                string TrashPathComplete = Path.Combine(TrashPath, newfilename);

                MimeMessage fileToAppend = Load_Mail(OldFilePath, Mail_ID);
                AppendToImapServer(fileToAppend);

                File.Move(OldPathComplete, TrashPathComplete);
            }
            catch (Exception ex)
                when (
                ex is IOException ||
                ex is FileNotFoundException ||
                ex is PathTooLongException ||
                ex is DirectoryNotFoundException) //File already exists
            {
                throw;
            }

            return true;
        }



        private void AppendToImapServer(MimeMessage MailToAppend)
        {
            using (ImapClient Client = new ImapClient())
            {
                try
                {
                    if (Client.IsConnected != true)
                    {
                        Client.Connect(User.Imap_Host, User.Imap_PORT, SecureSocketOptions.SslOnConnect);
                        Client.Authenticate(User.AccountUsername, User.AccountPassword);
                    }

                    IMailFolder trash = Client.GetFolder(SpecialFolder.Trash);


                    trash.Append(MailToAppend);

                }
                catch (Exception ex) when (ex is AuthenticationException ||
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentException)
                {
                    throw;
                }
            }
        }

        //Imap Connection and Authentication
        #region Connection & Authentication methods
        protected void ConnectAuth()
        {
            MailOPClient.Connect(User.Imap_Host, User.Imap_PORT, SecureSocketOptions.SslOnConnect);
            MailOPClient.Authenticate(User.AccountUsername, User.AccountPassword);
        }

        private void TestConnectAuth()
        {
            try
            {
                MailOPClient.Connect(User.Imap_Host, User.Imap_PORT, MailKit.Security.SecureSocketOptions.SslOnConnect);
                MailOPClient.Authenticate(User.AccountUsername, User.AccountPassword);
            }
            catch (Exception ex)
            when (
                ex is System.Net.Sockets.SocketException ||
                ex is ArgumentNullException ||
                ex is IOException ||
                ex is ArgumentException ||
                ex is ImapProtocolException ||
                ex is MailKit.Security.AuthenticationException ||
                ex is OperationCanceledException
                )
            {
                throw new CoreExceptions.ConnectionException();
            }
        }

        #endregion


        //Garbage collector
        //- add all objects here that are called with NEW operator
        #region Dispose methods (Garbage collecter methods)

        //Dispose methods for managing the resources !NEEDS IMPLEMENTATION!
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //TODO: dispose managed state (managed objects)
                    //client
                    MailOPClient.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ImapRecieve()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }
        public override void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
