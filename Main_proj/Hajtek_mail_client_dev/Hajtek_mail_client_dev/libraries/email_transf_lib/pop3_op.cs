﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using MailKit.Net.Pop3;
using Hajtek_mail_client_dev.libraries.exceptions;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{
    public class Pop3Recieve : Mail_POP3
    {
        protected Pop3Client ReceiveClient = new Pop3Client();
        protected int newEmailCount = 0;
        private UserData.UserInfo User;
        private string inboxFolder;

        public Pop3Recieve(UserData.UserInfo InitUser)
        {
            //Checks if the correct information is already inside the argument variable
            if (InitUser.AccountUsername == null &&
               InitUser.AccountPassword == null &&
               InitUser.Pop3_Host == null &&
               InitUser.Pop3_PORT == 0)
            {
                //Throws an exception since the arguments arent valid
                try
                {
                    throw new MailLibException();
                }
                catch (MailLibException.UserArgumentNULLException ex)
                {
                    //ensures that the exception thrown is actually the exception we want to recover from.
                    if (ex is MailLibException.UserArgumentNULLException)
                    {
                        //return from the constructor, such that it won't get initialized
                        return;
                    }
                }
            }

            //If no exception is thrown continue with initialization:
            else
            {
                //initializes the private user variable
                User = InitUser;
            }
        }

        public override int Receive_mail(ref UserData.UserInfo CurrActiveUser)
        {
            throw new NotImplementedException();
        }

        public override int Receive_mail(ref UserData.UserInfo user, bool specialfolders, params string[] foldernames)
        {
            throw new NotImplementedException();
        }

        //Dispose methods for managing the resources !NEEDS IMPLEMENTATION!
        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    ReceiveClient.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ImapRecieve()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }
        public override void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
