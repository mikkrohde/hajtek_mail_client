﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.exceptions;
using MailKit;
using MimeKit;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{
    //Mail_OP: Mail operations. 

    /// <summary>
    /// Different implementations Of recieve mail depending on the 
    /// </summary>

    /*  
     *  ALLE MAIL METODER SKAL HAVE IMPLEMENTERET ET IDISPOSABLE INTERFACE, DETTE ER C# GARBAGE COLLECTER
     *  ellers er Using() ubrugelig
     */

    /// <summary>
    /// Must contain a mail data structure with atleast the basic informations;
    /// Contains all abstract methods for the basic mail operations.
    /// - Send
    /// - Recieve
    /// - Reply
    /// - Deleting mail
    /// - Forward
    /// - Mark mail as read
    /// 
    /// The interface implements 
    /// 
    /// Mail_OP.
    /// 
    /// </summary>
    interface IMailOP : IDisposable
    {
        bool MoveToTrashFolder(string filepath, string Mail_ID);
        void LoadLocal_Inbox(ref UserData.Mailbox ListMailboxes);
        void LoadLocal_Sent(ref UserData.Mailbox ListMailboxes);
        void LoadLocal_Draft(ref UserData.Mailbox ListMailboxes);
        void LoadLocal_Spam(ref UserData.Mailbox ListMailboxes);
        void LoadLocal_Archive(ref UserData.Mailbox ListMailboxes);
        void LoadLocal_Trash(ref UserData.Mailbox ListMailboxes);
        MimeMessage Load_Mail(string folderpath, string filename);
        bool IsRead(string filename);
    }

    interface IRecieveMail : IDisposable
    {
        int Receive_mail(ref UserData.UserInfo CurrActiveUser);
        int Receive_mail(ref UserData.UserInfo user, bool specialfolders, params string[] foldernames);
    }

    interface ISendMail<T> : IDisposable
    {
        void Send_mail(string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI);
        void Reply_To_mail(List<T> mailboxList, int mailid, string subjectheaderUI, string mailtextbodyUI);
        void Forward_mail(List<T> mailboxList, int mailid, string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI);
    }


    public abstract class Mail_OP : IMailOP, IDisposable
    {
        public abstract bool MoveToTrashFolder(string filepath, string Mail_ID);
        public abstract MimeMessage Load_Mail(string folderpath, string filename);
        public abstract void LoadLocal_Inbox(ref UserData.Mailbox ListMailboxes);
        public abstract void LoadLocal_Sent(ref UserData.Mailbox ListMailboxes);
        public abstract void LoadLocal_Draft(ref UserData.Mailbox ListMailboxes);
        public abstract void LoadLocal_Spam(ref UserData.Mailbox ListMailboxes);
        public abstract void LoadLocal_Archive(ref UserData.Mailbox ListMailboxes);
        public abstract void LoadLocal_Trash(ref UserData.Mailbox ListMailboxes);
        public abstract void Dispose();
        public abstract bool IsRead(string filename);
    }

    public abstract class Mail_SMTP<T> : ISendMail<T>, IDisposable
    {
        public abstract void Send_mail(string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI);
        public abstract void Reply_To_mail(List<T> mailbox, int mailid, string subjectheaderUI, string mailtextbodyUI);
        public abstract void Forward_mail(List<T> mailbox, int mailid, string[] mailheaderUI, string subjectheaderUI, string mailtextbodyUI);

        public abstract void Dispose();
    }
    
    public abstract class Mail_IMAP : IRecieveMail, IDisposable
    {
        /* Main recieve method (fetches inbox)
         * - argument: none
         * + Returns: newmailsfetched
         */
        public abstract int Receive_mail(ref UserData.UserInfo CurrActiveUser);

        /* Secondary recieve method
         * 
         * - argument 1: string folder
         * 
         */
        public abstract int Receive_mail(ref UserData.UserInfo user, bool specialfolders, params string[] foldernames);
        public abstract void Dispose();

    }

    public abstract class Mail_POP3 : IRecieveMail, IDisposable
    {
        public abstract int Receive_mail(ref UserData.UserInfo CurrActiveUser);
        public abstract int Receive_mail(ref UserData.UserInfo user, bool specialfolders, params string[] foldernames);
        public abstract void Dispose();
    }

}
