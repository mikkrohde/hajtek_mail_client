﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit;
using MimeKit;
using MailKit.Net.Imap;
using email_data_structure.data_structures;

namespace Hajtek_mail_client_dev.libraries.email_transf_lib
{
    class Imap_mail_op : Mail_OP
    {

        public override void Receive_mail()
        {

        }

        public override void Delete_mail()
        {

        }

        public override void Get_mailbox()
        {
           
         //Inboxes = new ObservableCollection<InboxModel>();

         //int counter = 0;

        using (var imap_client = new ImapClient())
        {
             //User_Info.Data test = new User_Info.Data();

            imap_client.Connect(Mailbox_data_class.UserData[0].IMAP_Host, User_Info.Mailbox_data_class.UserData[0].IMAP_Port, User_Info.Mailbox_data_class.UserData[0].USE_SSL);

            imap_client.Authenticate(User_Info.Mailbox_data_class.UserData[0].AccountUsername, User_Info.Mailbox_data_class.UserData[0].AccountPassword);

            var inbox_folder = imap_client.Inbox;

            inbox_folder.Status(StatusItems.Unread);

            inbox_folder.Open(FolderAccess.ReadOnly);

            User_Info.Data.Unread_Emails_Inbox.Add(inbox_folder.Unread.ToString());
            User_Info.Data.Total_Emails_Inbox.Add(inbox_folder.Count);


            for (int i = inbox_folder.Count - 1; (inbox_folder.Count - 50) < i; i--)
            {
                    var message = inbox_folder.GetMessage(i);

                User_Info.Inbox_data e3 = new User_Info.Inbox_data();
                e3.Mail_ID = i;

                if (message.HtmlBody == null){
                    e3.HTML_Body = TextToHtml(message.TextBody);
                }else{
                        e3.HTML_Body = message.HtmlBody;
                }


            e3.Mail_To = User_Info.Mailbox_data_class.UserData[0].MailAddress;
            //e3.Mail_From = message.From.OfType<MailboxAddress>().Single().Name.ToString().Truncate_func(10);

            if (message.From.OfType<MailboxAddress>().Single().Name.ToString() == "")
            {
                e3.Mail_From = message.From.OfType<MailboxAddress>().Single().Address.ToString();
            }else{
                e3.Mail_From = message.From.OfType<MailboxAddress>().Single().Name.ToString();
            }

            //e3.Mail_From = message.From.ToString(); //message.From.OfType<MailboxAddress>().Single().Address.ToString();
            //e3.Mail_Subject = message.Subject;

            if (message.Subject == "")
            {
                e3.Mail_Subject = "No subject";
            }else{
                e3.Mail_Subject = message.Subject;
            }
                e3.Mail_Date_Time = message.Date.LocalDateTime.TimeOfDay.ToString().Substring(0, 5);
                User_Info.Mailbox_data_class.InboxData.Add(e3);

            }



                    imap_client.Disconnect(true);
                }
    }
}
