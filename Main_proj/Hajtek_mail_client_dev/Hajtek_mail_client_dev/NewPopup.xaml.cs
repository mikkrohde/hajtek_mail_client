﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hajtek_mail_client_dev.libraries.setup_lib;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.exceptions;

namespace Hajtek_mail_client_dev
{


    public partial class NewPopup : UserControl
    {

        public static bool _show_cefBrowser;

        public int NewReplyForward
        {
            get { return MainWindow.new_reply_forward; }
        }

        public int SelectedFolder
        {
            get { return MainWindow._selected_folder; }
        }

        public int GetMailID
        {
            get { return MainWindow.Mail_ID; }
        }

        public NewPopup()
        {
            InitializeComponent();

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                NewPopup_Title.Content = "Neuer Mail";
            }
            else
            {
                NewPopup_Title.Content = "New mail";
            }

        }

        private void Close_NewPopup_Click(object sender, RoutedEventArgs e)
        {
            //Lukker vinduet når man trykker på X (til venstre) 
            Visibility = Visibility.Hidden;
            

        }


        private void Send_Click(object sender, RoutedEventArgs e)
        {
            try { 
                if (NewReplyForward == 1)
                {
                    if(SelectedFolder == 1)
                    {
                        Core.SendMail<UserData.InboxData>(Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if(SelectedFolder == 3)
                    {
                        Core.SendMail<UserData.SpamData>(Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if(SelectedFolder == 4)
                    {
                        Core.SendMail<UserData.TrashData>(Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else
                    {
                        MessageBox.Show("You can not send an already sent mail. Use Forward.");
                    }
                
                }
            

                if (NewReplyForward == 2)
                {
                    if (SelectedFolder == 1)
                    {
                        Core.ReplyToMail(Core.CoreMailbox.Inboxdata, GetMailID, Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if (SelectedFolder == 2)
                    {
                        Core.ReplyToMail(Core.CoreMailbox.Sentdata, GetMailID, Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if (SelectedFolder == 3)
                    {
                        Core.ReplyToMail(Core.CoreMailbox.Spamdata, GetMailID, Subject_textbox.Text, Body_textbox.Text);
                    }
                    else
                    {
                        Core.ReplyToMail(Core.CoreMailbox.Trashdata, GetMailID, Subject_textbox.Text, Body_textbox.Text);
                    } 
                }


                if (NewReplyForward == 3)
                {
                    if (SelectedFolder == 1)
                    {
                        Core.ForwardMail(Core.CoreMailbox.Inboxdata, GetMailID, Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if (SelectedFolder == 2)
                    {
                        Core.ForwardMail(Core.CoreMailbox.Sentdata, GetMailID, Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else if (SelectedFolder == 3)
                    {
                        Core.ForwardMail(Core.CoreMailbox.Spamdata, GetMailID, Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                    else
                    {
                        Core.ForwardMail(Core.CoreMailbox.Trashdata, GetMailID, Email_textbox.Text.Split(','), Subject_textbox.Text, Body_textbox.Text);
                    }
                }
            }
            catch (CoreExceptions.ConnectionException)
            {
                MessageBox.Show("Currently Offline");
            }

            Email_textbox.Clear();
            Subject_textbox.Clear();
            Body_textbox.Clear();

            Visibility = Visibility.Hidden;

            _show_cefBrowser = true;
            
        }

    }
}
