﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Hajtek_mail_client_dev.Model;
using Hajtek_mail_client_dev;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using Hajtek_mail_client_dev.libraries.setup_lib;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Serialization;
using Hajtek_mail_client_dev.libraries.dir_lib.dir_mgmt_lib;
using System.IO;
using System.Xml.Linq;
using ListView = System.Windows.Controls.ListView;

namespace Hajtek_mail_client_dev
{
    /// <summary>
    /// Interaction logic for SwitchUser.xaml
    /// </summary>
    public partial class SwitchUser : System.Windows.Controls.UserControl
    {

        public ObservableCollection<SwitchUserModel> _usercollection = new ObservableCollection<SwitchUserModel>(); //Bruges til indbox cards i listen med mails
        public ObservableCollection<SwitchUserModel> UserCollection //Bruges til indbox cards i listen med mails, men gør det muligt at vælge mellem at vise mails for inbox, sent, spam og trash
        {
            get { return _usercollection; }
            set
            {
                _usercollection = value;
            }
        }

        public SwitchUser()
        {
            InitializeComponent();

            if (Core.CoreUserMBdata.GetUseGerman == true)
            {
                AddNewUser_Popup.First_name_textbox_backtext.Text = "Vorname";
                AddNewUser_Popup.Last_name_textbox_backtext.Text = "Nachname";
                AddNewUser_Popup.mail_address_textbox_backtext.Text = "E-Mail-Addresse";
                AddNewUser_Popup.Account_name_textbox_backtext.Text = "Benutzername";
                AddNewUser_Popup.Account_password_textbox_backtext.Text = "Passwort";
                AddNewUser_Popup.SMTP_host_textbox_backtext.Text = "SMTP Gastgeber";
                AddNewUser_Popup.SMTP_port_textbox_backtext.Text = "SMTP Anschlussstelle";
                AddNewUser_Popup.USE_SSL_SMTP_button.Content = "SSL für SMTP verwenden?";
                AddNewUser_Popup.IMAP_host_textbox_backtext.Text = "IMAP Gastgeber";
                AddNewUser_Popup.IMAP_port_textbox_backtext.Text = "IMAP Anschlussstelle";
                AddNewUser_Popup.USE_SSL_IMAP_button.Content = "SSL für IMAP verwenden?";
            }

            if (Properties.Settings.Default.FirstLaunch == false)
            {
                FirstTimeRunning();
            }

        }


        public void FirstTimeRunning()
        {
            string mainpath;

            try
            {
                mainpath = Path.Combine(System.Windows.Forms.Application.StartupPath, "PROFILES");
            }
            catch (System.Windows.Markup.XamlParseException ex)
            {
                return;
            }

            FileInfo[] FilesInDir;
            DirectoryInfo DirectoryInWhichToSearch = new DirectoryInfo(mainpath);

            try
            {
                FilesInDir = DirectoryInWhichToSearch.GetFiles();
            }
            catch (Exception ex)
            when (
            ex is System.Xaml.XamlParseException ||
            ex is IOException || ex is ArgumentNullException)
            {
                //rethrows for exception handling in Core
                throw;
            }

            foreach (var user in FilesInDir)
            {
                string full_path_name = user.FullName;
                string full_filename = user.Name;

                string[] filename_split = full_filename.Split('.');

                string UserName = filename_split[0];

                UserData.UserInfo tmp;

                using (FileStream fs = new FileStream(full_path_name, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(UserData.UserInfo));
                    tmp = (UserData.UserInfo)serializer.Deserialize(fs);
                }

                var avatar_image = new Uri(@"Assets/Avatars/" + tmp.AvatarName + ".png", UriKind.Relative);

                _usercollection.Add(new SwitchUserModel
                {
                    FullName = tmp.FullName,
                    EmailAddress = tmp.MailAddress,
                    AvatarName = avatar_image,
                    AccUsername = tmp.AccountUsername
                });
            }
        }

        private void SwitchUser_ListView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = (sender as ListView).SelectedItem;
            if(item != null)
            {
                SwitchUserModel @switch;

                try
                {
                    @switch = (SwitchUserModel)SwitchUser_ListView.SelectedItem;

                    var fullname = string.Concat(@switch.FullName.Where(c => !Char.IsWhiteSpace(c)));

                    Core.SwitchUser(fullname, @switch.AccUsername);

                    Core.RESTARTAPP();
                }
                catch (Exception ex)
                {
                    //throw;
                }
                
            }
            
        }

        private void SwitchUser_ListView_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        { 
            System.Windows.Controls.ContextMenu contextMenu = SwitchUser_ListView.TryFindResource("ListViewContextMenu") as System.Windows.Controls.ContextMenu;
            contextMenu.IsOpen = true;
        }

        private void Close_NewPopup_Click(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Hidden;
        }

        private void DeleteUserProfileOnRight_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NewUser_NewPopup_Click(object sender, RoutedEventArgs e)
        {
            AddNewUser_Popup.Visibility = Visibility.Visible;
        }
    }
}
//ItemContainerStyle="{StaticResource SwitchUserCard}"
//x:Key="ListViewContextMenu"