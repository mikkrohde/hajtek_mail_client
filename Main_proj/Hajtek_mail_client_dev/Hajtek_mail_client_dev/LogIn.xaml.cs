﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Hajtek_mail_client_dev.libraries.setup_lib;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Hajtek_mail_client_dev.data_structures.email_data_structure;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Hajtek_mail_client_dev
{
    //Meget minder om koderne fra de andre steder, så hvis der ikke stå nogen kommentar er det derfor
    public partial class LogIn : UserControl
    {
        public static string _first_name;

        public static Visibility _pick_avatar_Popup_Visibility_property = Visibility.Hidden;

        public bool Use_german
        {
            get { return Hajtek_mail_client_dev.Main._use_german; }
        }

        public ObservableCollection<bool> _useGerman= new ObservableCollection<bool>(); //Bruges til indbox cards i listen med mails
        public ObservableCollection<bool> UseGerman //Bruges til indbox cards i listen med mails, men gør det muligt at vælge mellem at vise mails for inbox, sent, spam og trash
        {
            get { return _useGerman; }
        }

        public LogIn()
        {    
            InitializeComponent();
        }

        private async void LogInPage_Loaded(object sender, RoutedEventArgs e)
        {
            var text = new LogIn_text_class();
            DataContext = text;

            if (Use_german == true)
            {
                text.First_name_text = "NEIN!";
                await Task.Delay(1);
                
            }
            else
            {
                text.First_name_text = "Welcome to your new mail client";
                await Task.Delay(2000);
                text.First_name_text = "NEIN!";
            }
        }

        public class LogIn_text_class : INotifyPropertyChanged
        {
            private string _first_name_text;
            
            public string First_name_text
            {
                get => _first_name_text; set
                {
                    _first_name_text = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LogIn.Use_german)));
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
        }

        private void Make_new_account_Click(object sender, RoutedEventArgs e)
        {

            try { 
                Core.CreateNewUser(USE_SSL_IMAP_button.IsChecked.Value,
                                   USE_SSL_SMTP_button.IsChecked.Value,
                                   Use_german,
                                   First_name_textbox.Text,
                                   Last_name_textbox.Text,
                                   mail_address_textbox.Text,
                                   Account_name_textbox.Text,
                                   Account_password_textbox.Password.ToString(),
                                   SMTP_host_textbox.Text,
                                   IMAP_host_textbox.Text,
                                   SMTP_port_textbox.Text,
                                   IMAP_port_textbox.Text,
                                   "null"
                                   );
                //Changes application property FirstLaunch variable.
                Properties.Settings.Default.FirstLaunch = false;
                Properties.Settings.Default.Save();

                Visibility = Visibility.Hidden;
            }
            catch (MailKit.Security.AuthenticationException)
            {
                MessageBox.Show("Failed To Authenticate. Try again.");
            }
            catch (System.Net.Sockets.SocketException)
            {
                MessageBox.Show("Failed To Connect. Check your account information");
            }
            catch(ArgumentException)
            {
                MessageBox.Show("Remember to enter information into all");
            }

            return;
        }

        private void Account_password_textbox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Account_password_textbox_backtext.Visibility = Visibility.Collapsed;
        }
    }
}
