﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hajtek_mail_client_dev
{
    /// <summary>
    /// Interaction logic for AddNewUser.xaml
    /// </summary>
    public partial class AddNewUser : UserControl
    {
        public bool _use_german;
        public bool Use_german
        {
            get { return _use_german; }
            set { _use_german = value; }  
        }

        public static string _avatar_picked;
        public string Avatar_picked
        {
            get { return _avatar_picked; }
            set { _avatar_picked = value; }
        }

        public AddNewUser()
        {
            InitializeComponent();
        }

        private void Cacel_new_account_Click(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Hidden;
        }

        private void Make_new_account_Click(object sender, RoutedEventArgs e)
        {
            
            try
            {
                Core.CreateNewUser(USE_SSL_IMAP_button.IsChecked.Value,
                                   USE_SSL_SMTP_button.IsChecked.Value,
                                   Use_german,
                                   First_name_textbox.Text,
                                   Last_name_textbox.Text,
                                   mail_address_textbox.Text,
                                   Account_name_textbox.Text,
                                   Account_password_textbox.Password.ToString(),
                                   SMTP_host_textbox.Text,
                                   IMAP_host_textbox.Text,
                                   SMTP_port_textbox.Text,
                                   IMAP_port_textbox.Text,
                                   Avatar_picked
                                   );
                //Changes application property FirstLaunch variable.
                Properties.Settings.Default.Save();

                Visibility = Visibility.Hidden;

                Core.RESTARTAPP();
            }
            catch (MailKit.Security.AuthenticationException)
            {
                MessageBox.Show("Failed To Authenticate. Try again.");
            }
            catch (System.Net.Sockets.SocketException)
            {
                MessageBox.Show("Failed To Connect. Check your account information");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Remember to enter information into all");
            }

            return;
        }

        private void Account_password_textbox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Account_password_textbox_backtext.Visibility = Visibility.Collapsed;
        }

        private void english_selected_Click(object sender, RoutedEventArgs e)
        {
            english_selected.Background = Brushes.DarkGray;
            german_selected.Background = Brushes.Transparent;
            Use_german = false;
        }

        private void german_selected_Click(object sender, RoutedEventArgs e)
        {
            german_selected.Background = Brushes.DarkGray;
            english_selected.Background = Brushes.Transparent;
            Use_german = true;
        }

        private void avatar1_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "1";
            avatar1.Background = Brushes.DarkGray;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar2_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "2";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.DarkGray;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar3_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "3";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.DarkGray;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar4_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "4";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.DarkGray;
            avatar5.Background = Brushes.Transparent;
        }

        private void avatar5_Click(object sender, RoutedEventArgs e)
        {
            Avatar_picked = "5";
            avatar1.Background = Brushes.Transparent;
            avatar2.Background = Brushes.Transparent;
            avatar3.Background = Brushes.Transparent;
            avatar4.Background = Brushes.Transparent;
            avatar5.Background = Brushes.DarkGray;
        }
    }
}
